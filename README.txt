ZPolyTrans
-- a library for computing and enumerating integer transformations of
   Z-polyhedra, based on PolyLib and Barvinok.
-- version 0.04, Release date: 2012/01/20

Main author:
 Rachid Seghir <rachid.seghir@gmail.com>
Other contributors:
 Vincent Loechner <loechner@unistra.fr>

ZPolyTrans is open-source software distributed under the terms of the
GNU General Public License. See the file COPYING for complete details on
the licensing.

For a guide to installation, please see INSTALL

The latest version of ZPolyTrans is available at
 https://gitlab.inria.fr/loechner/zpolytrans

USAGE:

1. The executable ./enumerate_image computes the elimination of some
integer variables from a union of parametric polyhedra.
It takes as input:

# number of (input polyhedra and variables to eliminate in each of them)

# constraints matrices of the input polyhedra
# (polylib format:
#  nb_rows nb_cols
#  nbrows *
#            eq(0)/ineq(1)  [ coef_i ]*  constant # >= 0
# )

# number of variables to be eliminated

# context constraints (constraints on the parameters; polylib format)

# parameter names (space separated, optional)


The variables of the input polyhedra are ordered as:
 variables to be eliminated; normal variables (to be kept); parameters.

The output is:
- a union of:
   - validity lattice
   - polyhedron
- the Ehrhart polynomial giving the number of integer points of the result.

There are some examples in the distribution, in ./examples/image/.
Run ./enumerate_image -h for a list of options.
Most common options are:
   -v: verbose, print resulting union of Z-polytopes
   -no-ep: don't compute Ehrhart polynomial of the result (implies -v)


2. The executable ./enumerate_zp_union computes the Ehrhart polynomial of
a union of parametric Z-polytopes.
It takes as input:

# number of input Z-polytopes

# a list of :
# - constraints matrix of the input polyhedron
# - integer matrix defining the lattice

# context constraints (polylib format)

# parameter names (space separated)

The output is:
- the Ehrhart polynomial giving the number of integer points of the result.

There are some examples in the distribution, in ./examples/zp_union/.
Run ./enumerate_zp_union -h for a list of options.

/*
 * This file is part of ZPolyTrans.

 ZPolyTrans is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 ZPolyTrans is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with ZPolyTrans.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Written by Rachid Seghir, 2005-2010 */
/* Modified by Vincent Loechner, 2011 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/times.h>
#include <barvinok/barvinok.h>
#include <barvinok/evalue.h>
#include <omp.h>

#include "Zenumerate.h"
#include "transform.h"

#if __STDC_VERSION__ < 199901L
# if __GNUC__ >= 2
#  define __func__ __FUNCTION__
# else
#  define __func__ "<unknown>"
# endif
#endif

#define ERREUR(x) do { fprintf(stderr, "%s (%s:%d): %s\n", __func__, __FILE__, __LINE__, x); exit(1); } while(0)

#define MAXRAYS   0

#define USING_ONE_DIM
#undef  PRINT_PERF

int main (int argc, char **argv)
{

  Polyhedron *P, *Q = NULL, *C, *PU = NULL;
  UZPolyhedron *ZP, *TZP, *ZPU = NULL;
  Matrix *M;
  int *NbExi, i, single = 0;
  int NbPoly;
#ifdef PRINT_PERF
  struct tms t1, t3;
  long ticks = sysconf (_SC_CLK_TCK);
#endif
  Enumeration *e, *en=NULL;
  evalue ev;
  const  char **param_name;
  int config_print_result=0, config_computeEP=1, config_debug=0, config_frac=1;
  double begin, end;

  for( i=1; i<argc ; i++ )
    {
      if( !strncmp( "-h", argv[i], 2 ) || !strncmp( "--help", argv[i], 6 ) )
	{
	  printf( "Usage: %s [-h] [-v] [-d] [-no-ep]\n", argv[0] );
	  printf( "  -h: this help message\n" );
	  printf( "  -v: verbose, print resulting union of Z-polytopes\n" );
	  printf( "  -d: debug\n" );
	  printf( "  -no-ep: don't compute Ehrhart polynomial of the result (implies -v)\n" );
	  printf( "  -periodic: use periodic numbers in Ehrhart polynomials (default: fractional)\n" );
	  exit(0);
	}
      else if( !strncmp( "-v", argv[i], 2 ) )
	{
	  config_print_result = 1;
	}
      else if( !strncmp( "-d", argv[i], 2 ) )
	{
	  config_debug = 1;
	}
      else if( !strncmp( "-no-ep", argv[i], 6 ) )
	{
	  config_computeEP = 0;
	  config_print_result = 1;
	}
      else if( !strncmp( "-periodic", argv[i], 9 ) )
	{
	  config_frac = 0;
	}
      else
	{
	  printf("Ignoring unrecognized option %s\n", argv[i] );
	}
    }

#ifdef PRINT_PERF
  times (&t1);
#endif

  if( config_debug )
    printf("reading number of input polyhedra\n");
  // read input
  NbPoly = Read_NbPoly ();
  if( NbPoly<=0 )
    ERREUR("NbPoly: Invalid input\n");

  if(! (NbExi = (int *)malloc (NbPoly * sizeof (int))) )
    ERREUR("malloc");


  if( config_debug )
    printf("reading input\n");
  for (i = 0; i < NbPoly; i++)
    {
      if( config_debug )
	printf("reading polyhedron %d\n",i+1);
      M = Matrix_Read ();
      NbExi[NbPoly-i-1] = Read_NbPoly ();
      if( NbExi[NbPoly-i-1] < 0 || NbExi[NbPoly-i-1] > M->NbColumns-2 )
	ERREUR("Invalid number of existential variables");
      PreChooseOrder (&M, NbExi[NbPoly-i-1]);
      P = Constraints2Polyhedron (M, MAXRAYS);
      if( !P )
	ERREUR("Invalid Input Polyhedron");
      Matrix_Free (M);
      P->next = PU;
      PU = P;
    }

  if( config_debug )
    printf("Reading Context\n");
  M = Matrix_Read ();
  C = Constraints2Polyhedron (M, MAXRAYS);
  Matrix_Free (M);
  if( !C )
    ERREUR("Context: Invalid Input Polyhedron");



  param_name = Read_ParamNames (stdin, C->Dimension);
  // end input

  // Vin: I replaced USING_ONE_DIM with (!config_print_result)
  //#ifdef USING_ONE_DIM
  if (NbPoly == 1 && !config_print_result )
    {
      if ((((NbExi[0]) - PU->NbEq) == 1))
	{
	  if (NbTests (PU) > 20)
	    {
	      if( config_debug )
		printf("Single=1, Projecting USING_ONE_DIM\n");
	      single = 1;
	      Q = ProjectOneDim (PU, NbExi[0]);
	    }
	}
    }
  //#endif //USING_ONE_DIM

  // ! single -> ! onedim
  if (!single)
    {
      if( config_debug )
	printf("!single\n");
      i = 0;
      for (P = PU; P; P = P->next)
	{
	  Q = NULL;
	  if (P->next)
	    {
	      Q = P->next;
	      P->next = NULL;
	    }
	  /*
	    Polyhedron_Print (stdout, P_VALUE_FMT, P);
	    printf ("\nNumber of existential variables : %d \n\n", NbExi[i]);
	    printf("Complexity = %d\n", P->Dimension+NbExi[i]-P->NbEq);
	    printf("Nb constraints = %d\n", P->NbConstraints);
	  */
	  if( config_debug )
	    {
	      printf("Projecting P:\n");
	      Polyhedron_Print(stdout, P_VALUE_FMT, P);
	    }

	  ZP = Project_Polytope (P, C, NbExi[i], NbPoly, !config_print_result);

	  //	  ZP = Project_Polytope (P, C, NbExi[i], NbPoly, 0);

	  if( config_debug )
	    {
	      printf("Result of projection:\n");
	      UZDomain_Print(ZP);
	    }

	  if (ZP != NULL)
	    {
	      for (TZP = ZP; TZP->next; TZP = TZP->next)
	        ;
	      TZP->next = ZPU;
	      ZPU = ZP;
	    }
	  i++;
	  if (Q)
	    P->next = Q;
	}

      if (ZPU == NULL)
	{
	  if( config_print_result )
	    printf("Empty\n");

	  if( config_computeEP )
	    {
	      value_init (ev.d);
	      value_init (ev.x.n);
	      value_set_si (ev.d, 1);
	      value_set_si (ev.x.n, 0);
	      en = (Enumeration *)malloc (sizeof (Enumeration));
	      en->ValidityDomain = Polyhedron_Copy (C);
	      en->EP = ev;
	      en->next = NULL;
	    }
	}
      else
	{
	  if( config_print_result )
	    {
	      UZDomain_Print(ZPU);
	      printf("\n");
	    }
	  if( config_computeEP )
	    {
	      if( config_debug )
		printf("Enumerating\n");

	      en = barvinok_Zenumerate (ZPU, C, param_name, MAXRAYS, config_frac);
    
	    }
	}
    }
  else //single = 1, config_print_result = 0, USING_ONE_DIM
    {
      if( config_debug )
	printf("single and using_one_dim\n");

      if( config_computeEP )
	{
	  if( config_debug )
	    printf("Enumerating\n");
	  en = Enumerate_Union (Q, C, param_name, MAXRAYS);
	}
    }

  if( config_computeEP )
    for (e = en; e; e = e->next)
      {
	Print_Domain (stdout, e->ValidityDomain, param_name);
	print_evalue (stdout, &e->EP, param_name);
	if (e->next)
	  printf ("\n");
      }

#ifdef PRINT_PERF
  times (&t3);
  printf("\n\nExecution time: ");
  printf ("usr: %g seconds  sys: %g  seconds \n",
	  (0.0 + t3.tms_utime - t1.tms_utime) / ticks,
	  (0.0 + t3.tms_stime - t1.tms_stime) / ticks);
#endif

  if( config_debug )
    printf("Freeing memory\n");

  Polyhedron_Free (C);
  Domain_Free (PU);
  UZDomain_Free (ZPU);
  if( en )
    Enumeration_free (en);
  free (NbExi);

  return 0;
}

/*
 * This file is part of ZPolyTrans.

    ZPolyTrans is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ZPolyTrans is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ZPolyTrans.  If not, see <http://www.gnu.org/licenses/>.
 */
/* Written by Rachid Seghir, 2005-2010 */
/* Modified by Vincent Loechner, 2011 */

#ifndef ZENUMERATE_H
#define ZENUMERATE_H

#define emptyQ2(P)    \
	                ((F_ISSET(P, POL_INEQUALITIES) && P->NbEq > P->Dimension) || \
		          (F_ISSET(P, POL_POINTS) && P->NbRays == 0))


typedef struct _SPolyhedron
{
  Value sign;
  Polyhedron *P;
  struct _SPolyhedron *next;
} SPolyhedron;

typedef struct _UZPolyhedron
{
  LatticeUnion *LU;
  Polyhedron *P;
  struct _UZPolyhedron *next;
} UZPolyhedron;

typedef struct _SZPolyhedron
{
  Value sign;
  UZPolyhedron *ZP;
  struct _SZPolyhedron *next;
} SZPolyhedron;



typedef struct _Periodic
{
  Value *val;
  unsigned *sizes;
  unsigned dim;
} Periodic;

typedef struct ZZPolyhedron
{
  LatticeUnion *Lat;
  Polyhedron *P;
  struct ZPolyhedron *next;
} ZZPolyhedron;

typedef struct _ResultP
{
  Polyhedron *Dark;
  Polyhedron *Coprime_P;
  ZPolyhedron *NonCoprime_P;
  Polyhedron *Context;
} ResultP;

typedef struct _SplitP
{
  Polyhedron *P;
  UZPolyhedron *UZP;
} SplitP;

extern Enumeration *barvinok_Zenumerate (UZPolyhedron * ZPU, Polyhedron * C,
				  const char **param_name, unsigned MAXRAYS,
				  int frac_set);
extern Enumeration *Enumerate_Union (Polyhedron * Q, Polyhedron * C,
			      const char **param_name, unsigned MAXRAYS);
extern UZPolyhedron *UZPolyhedron_Set (LatticeUnion * LU, Polyhedron * P);
extern LatticeUnion *LatUnionIntersection (LatticeUnion * L1, LatticeUnion * L2);
extern void PolyhedronSimplify (Polyhedron ** P, unsigned MaxR);
extern void SimplifyUZPoly (UZPolyhedron ** UZP, unsigned MaxR);
extern SZPolyhedron *SZPolyhedron_Alloc (Value v, UZPolyhedron * ZPol);
extern void NormalizeLattice (Matrix ** L, unsigned nb_free_v);
extern void UZDomain_Free (UZPolyhedron * UZP);
extern void UZDomain_Print (UZPolyhedron * UZP);
extern void Enumeration_free (Enumeration * en);
extern void SZDomain_Free (SZPolyhedron * ZPU);
extern int Read_NbPoly (void);
extern int Read_INT (void);

#endif

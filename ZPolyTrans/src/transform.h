/*
 * This file is part of ZPolyTrans.

    ZPolyTrans is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ZPolyTrans is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ZPolyTrans.  If not, see <http://www.gnu.org/licenses/>.
 */
/* Written by Rachid Seghir, 2005-2010 */
/* Modified by Vincent Loechner, 2011 */

#ifndef TRANSFORM_H
#define TRANSFORM_H


void PreChooseOrder (Matrix ** M, unsigned NbExi);
int NbTests (Polyhedron * P);
Polyhedron *ProjectOneDim (Polyhedron * P, int NbExi);
UZPolyhedron *Project_Polytope (Polyhedron * Pol, Polyhedron * C, int NbExi, int NbPoly, int using_one_dim);

#endif

/*
 * This file is part of ZPolyTrans.

    ZPolyTrans is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ZPolyTrans is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ZPolyTrans.  If not, see <http://www.gnu.org/licenses/>.
 */
/* Written by Rachid Seghir, 2005-2010 */
/* Modified by Vincent Loechner, 2011 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif


#include <assert.h>

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include <barvinok/barvinok.h>
#include <barvinok/util.h>
#include "Zenumerate.h"
#include <stdlib.h>
#include <ctype.h>
#include <omp.h>

static evalue evalue_makeperiodic( evalue e, int p );

static int Equal_evalues (evalue * ev1, evalue * ev2);

/* Creates and sets a structure UZPolyhedron which is the inersection 
 * of a polyhedron and a union of lattices. This function does not do 
 * a copy of sturctures LU and P */

UZPolyhedron *UZPolyhedron_Set (LatticeUnion * LU, Polyhedron * P)
{
  UZPolyhedron *res = malloc (sizeof (UZPolyhedron));
  if (!res)
    {
      fprintf (stderr, " UZPolyhedron_Set : out of memory space");
      return 0;
    }
  res->LU = LU;
  res->P = P;
  res->next = NULL;
  return res;
}

void UZDomain_Print (UZPolyhedron * UZP)
{
  UZPolyhedron *P;
  if (!UZP)
    printf ("\n < NULL UZPolyhedron > \n");
  for (P = UZP; P; P = P->next)
    {
      PrintLatticeUnion (stdout, P_VALUE_FMT, P->LU);
      Polyhedron_Print (stdout, P_VALUE_FMT, P->P);
    }
  return;
}

void LatticeUnion_Free2 (LatticeUnion * Head)
{
  LatticeUnion *temp, *t;
  temp = Head;
  while (temp)
    {
      t = temp->next;
      Matrix_Free (temp->M);
      free (temp);
      temp = t;
    }
  return;
}

void UZP_Free (UZPolyhedron * UZP)
{
  if (!UZP)
    return;
  LatticeUnion_Free2 (UZP->LU);
  Domain_Free (UZP->P);
  free (UZP);
  return;
}

void UZDomain_Free (UZPolyhedron * UZP)
{
  UZPolyhedron *Next;
  for (; UZP; UZP = Next)
    {
      Next = UZP->next;
      UZP_Free (UZP);
    }
  return;
}

ZPolyhedron *ZPolyhedron_Copy (ZPolyhedron * A)
{
  ZPolyhedron *Zpol;
  Zpol = ZPolyhedron_Alloc (A->Lat, A->P);
  return Zpol;
}

/* Creates and sets a structure SZPolyhedron which is a signed intersection
 * of a polyhedron and a union of lattices. This function does not make a copy 
 * of sturcture ZPol */

SZPolyhedron *SZPolyhedron_Alloc (Value v, UZPolyhedron * ZPol)
{
  SZPolyhedron *SZP = malloc (sizeof (SZPolyhedron));
  value_init (SZP->sign);
  value_assign (SZP->sign, v);
  SZP->ZP = ZPol;
  SZP->next = NULL;
  return SZP;
}

void SZDomain_Free (SZPolyhedron * ZPU)
{
  SZPolyhedron *ZP, *T;

  ZP = ZPU;
  while (ZP)
    {
      T = ZP->next;
      value_clear (ZP->sign);
      UZDomain_Free (ZP->ZP);
      free (ZP);
      ZP = T;
    }
}

void SDomain_Free (SPolyhedron * SPU)
{
  SPolyhedron *SP, *T;
  SP = SPU;
  while (SP)
    {
      T = SP->next;
      value_clear (SP->sign);
      Domain_Free (SP->P);
      free (SP);
      SP = T;
    }
}

LatticeUnion *LatUnionIntersection (LatticeUnion * L1, LatticeUnion * L2)
{
  LatticeUnion *res = NULL, *L, *LL, *LT;

  if (L1 == NULL || L2 == NULL)
    {
      fprintf (stderr,
	       "LatUnionIntersection: Intersection with NULL lattice");
      return NULL;
    }
  for (L = L1; L; L = L->next)
    {
      for (LL = L2; LL; LL = LL->next)
	{
	  LT = LatticeUnion_Alloc ();
	  LT->M = LatticeIntersection (L->M, LL->M);
	  if (isEmptyLattice (LT->M))
	    {
	      LatticeUnion_Free (LT);
	    }
	  else
	    {
	      LT->next = res;
	      res = LT;
	    }
	}
    }
  return res;
}

/* Removes from structure UZPolyhedron the lattices whose the intersection 
 * with the polyhedron is empty */

void SimplifyUZPoly (UZPolyhedron ** UZP, unsigned MaxR)
{
  UZPolyhedron *U = (*UZP);
  LatticeUnion *LU, *prev = NULL;
  Polyhedron *P;

  LU = U->LU;
  while (LU)
    {
      P = Polyhedron_Preimage (U->P, LU->M, MaxR);
      PolyhedronSimplify (&P, MaxR);
      if (!emptyQ (P))
	{
	  prev = LU;
	  LU = LU->next;
	}
      else
	{
	  if (!prev)
	    {
	      U->LU = LU->next;
	      LU->next = NULL;
	      LatticeUnion_Free (LU);
	      LU = U->LU;
	    }
	  else
	    {
	      prev->next = LU->next;
	      LU->next = NULL;
	      LatticeUnion_Free (LU);
	      LU = prev->next;
	    }
	}
      Domain_Free (P);
    }
  if (!U->LU)
    {
      Domain_Free (U->P);
      free (U);
      *UZP = NULL;
    }
}

int Read_NbPoly (void)
{
  char *c, s[1024], str[1024];
  int n;
  int u;
  Value v;
  value_init (v);
  do
    {
      c = fgets (s, 1024, stdin);
      while (c && isspace (*c) && *c != '\n')
	++c;
    }
  while (c && (*c == '#' || *c == '\n'));
  if (!c || *c == '\n' || *c == '#')
    {
      errormsg1 ("Read_NbExist", "badNb_Exist",
		 "Bad number of existential variables");
      return -1;
    }
  sscanf (c, "%s%n", str, &n);
  value_read (v, str);
  u = VALUE_TO_INT (v);
  value_clear (v);
  return u;
}

/* Returns the union of all validity domains in structure en */

static Polyhedron *DMUnion (Enumeration * en, unsigned MaxR)
{
  Enumeration *e1;
  Polyhedron *d, *tmp;;

  d = Empty_Polyhedron (en->ValidityDomain->Dimension);
  for (e1 = en; e1; e1 = e1->next)
    {
      tmp = d;
      d = DomainUnion (d, e1->ValidityDomain, MaxR);
      Domain_Free (tmp);
    }
  return d;
}

/* Checks whether a polyhedron is included in one of the validity domains of 
 * structure en */

static int IncludeInRes (Polyhedron * p, Enumeration * en, unsigned MR)
{
  Enumeration *e;
  for (e = en; e; e = e->next)
    {
      if (PolyhedronIncludes (e->ValidityDomain, p))
	return 1;
    }
  return 0;
}

static void Floor (Value * c, Value * num, Value * den)
{
  Value cei;
  value_init (cei);
  value_modulus (cei, *num, *den);
  if (value_neg_p (cei))
    value_addto (cei, cei, *den);
  value_substract (cei, *num, cei);
  value_division (*c, cei, *den);
  value_clear (cei);
}

/* Evaluates an Ehrhart polynomial for given parameter values */

Value *compute_poly2 (Enumeration * en, Value * list_args)
{
  double res;
  Value *tmp;
  tmp = malloc (sizeof (Value));
  assert (tmp != NULL);
  value_init (*tmp);
  value_set_si (*tmp, 0);

  if (!en)
    return (tmp);		/* no ehrhart polynomial */
  if (en->ValidityDomain)
    {
      if (!en->ValidityDomain->Dimension)
	{			/* no parameters */
	  res = compute_evalue (&en->EP, list_args);
	  if (res >= 0)
	    {
	      value_set_double (*tmp, res + .25);
	    }
	  else
	    {
	      value_set_double (*tmp, res - .25);
	    }
	  return (tmp);
	}
    }
  else
    return (tmp);		/* no Validity Domain */
  while (en)
    {
      if (in_domain (en->ValidityDomain, list_args))
	{

#ifdef EVAL_EHRHART_DEBUG
	  Print_Domain (stdout, en->ValidityDomain);
	  print_evalue (stdout, &en->EP);
#endif

	  res = compute_evalue (&en->EP, list_args);
	  if (res >= 0)
	    {
	      value_set_double (*tmp, res + .25);
	    }
	  else
	    {
	      value_set_double (*tmp, res - .25);
	    }
	  return (tmp);
	}
      else
	en = en->next;
    }
  value_set_si (*tmp, 0);
  return (tmp);			/* no compatible domain with the arguments */
}				/* compute_poly */

/* Checks whether a validity domain is simply an integer point*/

int vertex_domains (Polyhedron * P)
{
  Polyhedron *Q;
  int res = 1;
  for (Q = P; Q; Q = Q->next)
    {
      if (Q->NbRays != 1 || value_zero_p (Q->Ray[0][Q->Dimension + 1]))
	return 0;
    }
  return res;
}

/* Replaces, when possible, ajacent validity domains by their convex hull */

Polyhedron *Domain_fusion (Polyhedron * P, unsigned MaxR)
{
  Polyhedron *Conv, *Diff, *res;
  Conv = DomainConvex (P, MaxR);
  Diff = DomainDifference (Conv, P, MaxR);
  res = DomainDifference (Conv, Diff, MaxR);
  return res;
}

/* Simplifies an Enumeration structure to reduce its size when possible */

void Simplify_Enumeration (Enumeration ** en, const char **pn)
{
  Enumeration *e, *ten, *ren = NULL, *tmp, *prev = NULL;
  Polyhedron *P, *Q, *TP;
  Vector *V = Vector_Alloc ((*en)->ValidityDomain->Dimension);
  evalue *ev;
  Value gcd;
  int i, first = 1, bl;

  value_init (gcd);
  e = (*en);
  while (e != NULL)
    {
      P = e->ValidityDomain;

      if (vertex_domains (P))
	{
	  if (P->next == NULL)
	    {
	      bl = 1;
	      for (i = 1; i <= P->Dimension; i++)
		{
		  if (value_notzero_p (P->Ray[0][1]))
		    {
		      bl = 0;
		      break;
		    }
		}
	      if (bl)
		value_set_si (P->Ray[0][P->Dimension + 1], 1);

	      if (value_notone_p (P->Ray[0][P->Dimension + 1]))
		{
		  Vector_Gcd (P->Ray[0] + 1, P->Dimension, &gcd);
		  Gcd (gcd, P->Ray[0][P->Dimension + 1], &gcd);
		  if (value_notone_p (gcd))
		    {
		      for (i = 1; i <= P->Dimension + 1; i++)
			  value_division (P->Ray[0][i], P->Ray[0][i], gcd);
		    }
		}

	      if (value_one_p (P->Ray[0][P->Dimension + 1]))
		{
		  if (value_zero_p (e->EP.d))
		    {
		      Vector_Copy (P->Ray[0] + 1, V->p, P->Dimension);
		      if (value_zero_p (e->EP.d))
			{
			  ev = malloc (sizeof (evalue));
			  value_init (ev->d);
			  value_init (ev->x.n);
			  value_set_si (ev->d, 1);
			  value_assign (ev->x.n, *compute_poly2 (e, V->p));
			  free_evalue_refs (&e->EP);
			  e->EP = *ev;
			}
		    }
		  prev = e;
		  e = e->next;
		  if (first)
		    first = 0;
		}
	      else
		{
		  tmp = e->next;
		  e->next = NULL;
		  Enumeration_free (e);
		  if (first)
		    {
		      *en = tmp;
		      e = tmp;
		    }
		  else
		    {
		      prev->next = tmp;
		      e = tmp;
		    }
		}
	    }
	  else
	    {
	      for (Q = P; Q; Q = Q->next)
		{
		  bl = 1;
		  TP = Q->next;
		  Q->next = NULL;
		  for (i = 1; i <= Q->Dimension; i++)
		    {
		      if (value_notzero_p (Q->Ray[0][1]))
			{
			  bl = 0;
			  break;
			}
		    }
		  if (bl)
		    value_set_si (Q->Ray[0][Q->Dimension + 1], 1);

		  if (value_notone_p (Q->Ray[0][Q->Dimension + 1]))
		    {
		      Vector_Gcd (Q->Ray[0] + 1, Q->Dimension, &gcd);
		      Gcd (gcd, Q->Ray[0][Q->Dimension + 1], &gcd);
		      if (value_notone_p (gcd))
			{
			  for (i = 1; i <= Q->Dimension + 1; i++)
			    value_division (Q->Ray[0][i], Q->Ray[0][i], gcd);
			}
		    }

		  if (value_one_p (Q->Ray[0][Q->Dimension + 1]))
		    {
		      Vector_Copy (Q->Ray[0] + 1, V->p, Q->Dimension);
		      ten = malloc (sizeof (Enumeration));
		      ten->next = NULL;
		      ev = malloc (sizeof (evalue));
		      value_init (ev->d);
		      value_init (ev->x.n);
		      value_set_si (ev->d, 1);
		      ten->EP = e->EP;
		      ten->ValidityDomain = Q;
		      value_assign (ev->x.n, *compute_poly2 (ten, V->p));
		      ten->EP = *ev;
		      ten->ValidityDomain = Polyhedron_Copy (Q);
		      ten->next = ren;
		      ren = ten;
		    }
		  Q->next = TP;
		}

	      tmp = e->next;
	      e->next = NULL;
	      Enumeration_free (e);
	      if (first)
		{
		  *en = tmp;
		  e = tmp;
		}
	      else
		{
		  prev->next = tmp;
		  e = tmp;
		}
	    }
	}
      else
	{
	  prev = e;
	  e = e->next;
	  if (first)
	    first = 0;
	}
    }
  if (ren)
    {
      for (ten = ren; ten->next; ten = ten->next);
      ten->next = (*en);
      (*en) = ren;
    }
  value_clear (gcd);
  Vector_Free (V);
}

/* Simplifies a polyhedron in order to get an empty polyhedron for all 
 * polyhedra having no inter point */

void PolyhedronSimplify (Polyhedron ** P, unsigned MaxR)
{
  Value gcd, tmp;
  Polyhedron *q, *p = *P;
  unsigned dim = p->Dimension;
  int i, bl;

  Matrix *M = Matrix_Alloc (p->NbConstraints, dim + 2);
  value_init (gcd);
  value_init (tmp);
gto:
  if (emptyQ (p))
    {
      return;
      Matrix_Free (M);
      value_clear (gcd);
      value_init (tmp);
    }
  bl = 0;
  for (i = 0; i < p->NbConstraints; i++)
    {
      Vector_Copy (p->Constraint[i], M->p[i], dim + 2);
      Vector_Gcd (M->p[i] + 1, dim, &gcd);
      if (value_notone_p (gcd))
	{
	  bl = 1;
	  value_modulus (tmp, M->p[i][dim + 1], gcd);

	  if (value_zero_p (tmp))
	    {
	      Vector_AntiScale (M->p[i] + 1, M->p[i] + 1, gcd, dim + 1);
	    }
	  else
	    {
	      if (value_notzero_p (M->p[i][0]))
		{
		  Vector_AntiScale (M->p[i] + 1, M->p[i] + 1, gcd, dim);
		  Floor (&M->p[i][dim + 1], &M->p[i][dim + 1], &gcd);
		}
	      else
		{
		  Matrix_Free (M);
		  value_clear (tmp);
		  value_clear (gcd);
		  Domain_Free (p);
		  *P = Empty_Polyhedron (dim);
		  return;
		}
	    }
	}
    }
  if (bl)
    {
      q = Constraints2Polyhedron (M, MaxR);
      Domain_Free (p);
      *P = p = q;
      goto gto;
    }
  Matrix_Free (M);
  value_clear (tmp);
  value_clear (gcd);
  return;
}

/* Calculates the sum of two piecewise Ehrhart polynomials (Enumeration structures) */

Enumeration *Enumeration_Union (Enumeration * en1, Enumeration * en2, unsigned MaxR)
{
  Enumeration *e1, *e2, *tmp, *res = NULL;
  Polyhedron *d1, *d2, *d;
  d1 = DMUnion (en1, MaxR);
  d2 = DMUnion (en2, MaxR);
  for (e1 = en1; e1; e1 = e1->next)
    {
      for (e2 = en2; e2; e2 = e2->next)
	{
	  d = DomainIntersection (e1->ValidityDomain, e2->ValidityDomain, MaxR);
	  if (d && !emptyQ (d) && !IncludeInRes (d, res, MaxR))
	    {
	      evalue ev;
	      value_init (ev.d);
	      value_assign (ev.d, e2->EP.d);
	      if (value_zero_p (ev.d))
		{
		  ev.x.p = ecopy (e2->EP.x.p);
		}
	      else
		{
		  value_init (ev.x.n);
		  value_assign (ev.x.n, e2->EP.x.n);
		}

	      if( (value_zero_p(e1->EP.d) && e1->EP.x.p->type==periodic)
		         && !(value_zero_p(ev.d) && ev.x.p->type == periodic) )
		ev = evalue_makeperiodic( ev, e1->EP.x.p->pos );
	      else if( !(value_zero_p(e1->EP.d) && e1->EP.x.p->type==periodic)
		         && value_zero_p(ev.d) && ev.x.p->type == periodic )
		e1->EP = evalue_makeperiodic( e1->EP, ev.x.p->pos );

	      eadd (&e1->EP, &ev);
	      reduce_evalue (&ev);
	      tmp = malloc (sizeof (Enumeration));
	      tmp->ValidityDomain = d;
	      tmp->EP = ev;
	      tmp->next = res;
	      res = tmp;
	    }
	}
      d = DomainDifference (e1->ValidityDomain, d2, MaxR);
      if (d && !emptyQ (d) && !IncludeInRes (d, res, MaxR))
	{
	  tmp = malloc (sizeof (Enumeration));
	  tmp->ValidityDomain = d;
	  value_init (tmp->EP.d);
	  value_assign (tmp->EP.d, e1->EP.d);
	  if (value_zero_p (tmp->EP.d))
	    {
	      tmp->EP.x.p = ecopy (e1->EP.x.p);
	    }
	  else
	    {
	      value_init (tmp->EP.x.n);
	      value_assign (tmp->EP.x.n, e1->EP.x.n);
	    }
	  tmp->next = res;
	  res = tmp;
	}
    }
  for (e2 = en2; e2; e2 = e2->next)
    {
      d = DomainDifference (e2->ValidityDomain, d1, MaxR);
      if (d && !emptyQ (d) && !IncludeInRes (d, res, MaxR))
	{
	  tmp = malloc (sizeof (Enumeration));
	  tmp->ValidityDomain = d;
	  value_init (tmp->EP.d);
	  value_assign (tmp->EP.d, e2->EP.d);
	  if (value_zero_p (tmp->EP.d))
	    {
	      tmp->EP.x.p = ecopy (e2->EP.x.p);
	    }
	  else
	    {
	      value_init (tmp->EP.x.n);
	      value_assign (tmp->EP.x.n, e2->EP.x.n);
	    }
	  tmp->next = res;
	  res = tmp;
	}
    }

  Domain_Free (d1);
  Domain_Free (d2);
  return (res);
}

void Enumeration_free (Enumeration * en)
{
  Enumeration *ee;
  while (en)
    {
      free_evalue_refs (&(en->EP));
      Domain_Free (en->ValidityDomain);
      ee = en->next;
      free (en);
      en = ee;
    }
}

/* Creates a lookup-table binary periodic number (pos mob V[0] = V[1]). 
 * Such a number is required to count integer points in Z-polytopes 
 * whose lattice depends on a parameter in position 'pos' */

evalue *bin_evalue (Vector * V, evalue * evbin, unsigned pos)
{
  evalue *eval;

  int per = VALUE_TO_INT (V->p[0]), cte, i;
  Value tmp;
  value_init (tmp);
  value_modulus (tmp, V->p[1], V->p[0]);
  if value_neg_p (tmp) 
     value_addto (tmp, tmp, V->p[0]);

  cte = VALUE_TO_INT (tmp);

  eval = malloc (sizeof (evalue));
  value_init (eval->d);
  value_set_si (eval->d, 0);
  eval->x.p = new_enode (periodic, per, pos);
  for (i = 0; i < per; i++)
    {
      if (i == cte)
	{
	  eval->x.p->arr[i] = *evbin;
	}
      else
	{
	  value_init (eval->x.p->arr[i].d);
	  value_set_si (eval->x.p->arr[i].d, 1);
	  value_init (eval->x.p->arr[i].x.n);
	  value_set_si (eval->x.p->arr[i].x.n, 0);
	}
    }
  value_clear (tmp);
  return eval;
}

/* Makes parameters befor variables in a matrix defining a lattice. 
 * This is required to keep parameters independent of the variables
 * when the lattice is lower triangularized */

Matrix *InverseParamOrder (Matrix * M, unsigned n_par)
{
  int i, j, n_var = M->NbRows - n_par;
  Matrix *L = Matrix_Alloc (M->NbRows, M->NbRows), *T;

  for (i = 0; i < n_par; i++)
    {
      for (j = 0; j < M->NbColumns; j++)
	   value_assign (L->p[i][j], M->p[i + n_var][j]);
    }
  for (i = 0; i < n_var; i++)
    {
      for (j = 0; j < M->NbColumns; j++)
	   value_assign (L->p[i + n_par][j], M->p[i][j]);
    }

  T = Matrix_Alloc (L->NbRows, L->NbRows);

  for (i = 0; i < n_par; i++)
    {
      for (j = 0; j < L->NbRows; j++)
	   value_assign (T->p[j][i], L->p[j][i + n_var]);
    }

  for (i = 0; i < n_var; i++)
    {
      for (j = 0; j < L->NbRows; j++)
	   value_assign (T->p[j][i + n_par], L->p[j][i]);
    }
  Matrix_Free (L);
  return T;
}

/* Returns to the original order of varibles and parameters after
 * lower triangularization of a lattice */

Matrix *FirstParamOrder (Matrix * M, unsigned n_par)
{
  int i, j, n_var = M->NbRows - n_par;
  Matrix *L = Matrix_Alloc (M->NbRows, M->NbRows), *T;

  for (i = 0; i < n_var; i++)
    {
      for (j = 0; j < M->NbColumns; j++)
	   value_assign (L->p[i][j], M->p[i + n_par][j]);
    }
  for (i = 0; i < n_par; i++)
    {
      for (j = 0; j < M->NbColumns; j++)
	   value_assign (L->p[i + n_var][j], M->p[i][j]);
    }

  T = Matrix_Alloc (L->NbRows, L->NbRows);

  for (i = 0; i < n_var; i++)
    {
      for (j = 0; j < L->NbRows; j++)
	   value_assign (T->p[j][i], L->p[j][i + n_par]);
    }

  for (i = 0; i < n_par; i++)
    {
      for (j = 0; j < L->NbRows; j++)
	   value_assign (T->p[j][i + n_var], L->p[j][i]);
    }
  Matrix_Free (L);
  return T;
}

/* Returns the linear part of a lattice */

Matrix *Linear_Part (Matrix * M)
{
  Matrix *L = Matrix_Alloc (M->NbRows - 1, M->NbColumns - 1);
  int i;
  for (i = 0; i < L->NbRows; i++)
    Vector_Copy (M->p[i], L->p[i], L->NbColumns);
  return L;
}

/* Returns to the original parameters after the variable compression step. 
 * The regular variables are keeped compressed */

Polyhedron *GetOriginalParams (Polyhedron * P, Matrix * hh, unsigned MaxR)
{
  Polyhedron *Q;
  unsigned dim = P->Dimension, n_par = hh->NbRows - 1;
  int i, j, n_var = dim - hh->NbRows + 1;
  Value tmp, val;

  value_init (tmp);
  value_init (val);
  Matrix *M = Matrix_Alloc (P->NbConstraints, dim + 2);
  for (i = 0; i < P->NbConstraints; i++)
    Vector_Copy (P->Constraint[i], M->p[i], dim + 2);
  for (j = 0; j < n_par; j++)
    {
      if (value_notone_p (hh->p[j][n_par + 1]))
	{
	  for (i = 0; i < P->NbConstraints; i++)
	    {
	      if (value_notzero_p (M->p[i][n_var + 1 + j]))
		{
		  value_assign (tmp, M->p[i][n_var + 1 + j]);
		  Vector_Scale (M->p[i] + 1, M->p[i] + 1,
				hh->p[j][n_par + 1], dim + 1);
		  value_multiply (val, tmp, hh->p[j][n_par]);
		  value_addto (M->p[i][dim + 1], M->p[i][dim + 1], val);
		  value_multiply (val, tmp, hh->p[j][j]);
		  value_assign (M->p[i][n_var + 1 + j], val);
		}
	    }
	}
      else
	{
	  for (i = 0; i < P->NbConstraints; i++)
	    {
	      if (value_notzero_p (M->p[i][n_var + 1] + j))
		{
		  value_assign (tmp, M->p[i][n_var + 1 + j]);
		  value_multiply (val, tmp, hh->p[j][n_par]);
		  value_addto (M->p[i][dim + 1], M->p[i][dim + 1], val);
		  value_multiply (val, tmp, hh->p[j][j]);
		  value_assign (M->p[i][n_var + 1 + j], val);
		}
	    }

	}
    }
  Q = Constraints2Polyhedron (M, MaxR);
  Matrix_Free (M);
  value_clear (tmp);
  value_clear (val);
  return Q;

}

/* Checks whether the parameters are independent of the variables in the 
 * matrix defining a lattice */

int PVarsIndependent (Matrix * M, unsigned n_var, unsigned n_par)
{
  int i, j, bl = 1;
  for (i = n_var; i < n_var + n_par; i++)
    {
      for (j = 0; j < n_var; j++)
	{
	  if (value_notzero_p (M->p[i][j]))
	    {
	      bl = 0;
	      break;
	    }
	}
      if (!bl)
	break;
    }
  return bl;
}

/* Checks whether two enodes are "syntaxicly" equal */

int Equal_enodes (enode * e1, enode * e2)
{
  int i;
  if (e1->type != e2->type)
    {
      return 0;
    }
  else
    {
      if (e1->pos != e2->pos)
	{
	  return 0;
	}
      else
	{
	  if (e1->size != e2->size)
	    {
	      return 0;
	    }
	  else
	    {
	      for (i = 0; i < e1->size; i++)
		{
		  if (!Equal_evalues (&e1->arr[i], &e2->arr[i]))
		    {
		      return 0;
		    }
		}
	      return 1;
	    }
	}
    }
}

/* Checks whether two evalues (Ehrhart polynomials) are "syntaxicly" equal */

int Equal_evalues (evalue * ev1, evalue * ev2)
{
  if (value_ne (ev1->d, ev2->d))
    {
      return 0;
    }
  else
    {
      if (value_notzero_p (ev1->d))
	{
	  if (value_ne (ev1->x.n, ev2->x.n))
	    {
	      return 0;
	    }
	  else
	    {
	      return 1;
	    }
	}
      else
	{
	  return Equal_enodes (ev1->x.p, ev2->x.p);
	}
    }

}

/* Simplifies an Enumeration structure by gathering polynomials that 
 * are equal, and, when possible, polynomials having one integer point as 
 * validity domain */

void Enumeration_fusion (Enumeration ** en, unsigned MaxR)
{
  Enumeration *prev, *ee, *e, *t, *tmp1, *tmp2;
  Polyhedron *P, *Q;
  int bl, bl1;

  if (!(*en))
    return;
  Vector *V = Vector_Alloc ((*en)->ValidityDomain->Dimension);
  prev = NULL;
  e = *en;
  while (e)
    {
      t = e->next;
      if (value_notzero_p (e->EP.d) && value_zero_p (e->EP.x.n) && (*en)->next!=NULL)
	{
	  if (prev == NULL)
	    {
	      *en = e->next;
	      e->next = NULL;
	      Enumeration_free (e);
	    }
	  else
	    { 
	      prev->next = e->next;
	      e->next = NULL;
	      Enumeration_free (e);
	    } 
	}
      else
	{
	  prev = e;
	}
      e = t;
    } 
   if (!(*en))
    return;
  if ((*en)->next != NULL)
    {
      prev = NULL;
      e = *en;
      while (e)
	{
	  bl = 1;
	  tmp1 = e;
	  tmp2 = e->next;
	  Q = e->ValidityDomain;
	  if (Q->next == NULL && Q->NbRays == 1 
	         && value_notzero_p (Q->Ray[0][Q->Dimension + 1]))
	    {
	      assert (value_one_p (Q->Ray[0][Q->Dimension + 1]));
	      if (prev != NULL)
		{
		  prev->next = e->next;
		  e->next = *en;
		  *en = e;
		  bl = 0;
		}
	    }
	  if (bl)
	    prev = tmp1;
	  e = tmp2;
	}
      prev = NULL;
      e = *en;
      while (e->next)
	{
	  bl = 0;
	  bl1 = 0;
	  t = e->next;
	  for (ee = e->next; ee; ee = ee->next)
	    {
	      if (Equal_evalues (&e->EP, &ee->EP))
		{
		  P = ee->ValidityDomain;
		  ee->ValidityDomain =
		    DomainUnion (P, e->ValidityDomain, MaxR);
		  Domain_Free (P);
		  bl = 1;
		  break;
		}
	      else
		{
		  P = e->ValidityDomain;
		  if (P->next == NULL && P->NbRays == 1
                     && value_notzero_p (P->Ray[0][P->Dimension + 1]))
		    {
		      Vector_Copy (P->Ray[0] + 1, V->p, P->Dimension);
		      tmp1 = e->next;
		      e->next = NULL;
		      tmp2 = ee->next;
		      ee->next = NULL;
		      P = ee->ValidityDomain;
		      ee->ValidityDomain = DomainUnion (P, e->ValidityDomain, MaxR);
		      if (value_eq (*compute_poly2 (e, V->p), *compute_poly2 (ee, V->p)))
			{
			  Domain_Free (P);
			  bl = 1;
			  e->next = tmp1;
			  ee->next = tmp2;
			  break;
			}
		      else
			{
			  Q = ee->ValidityDomain;
			  ee->ValidityDomain = P;
			  Domain_Free (Q);
			}
		      e->next = tmp1;
		      ee->next = tmp2;
		    }
		}
	    }
	  if (bl)
	    {
	      if (prev == NULL)
		{
		  *en = e->next;
		  bl1 = 1;
		}
	      else
		{
		  prev->next = e->next;
		}
	      e->next = NULL;
	      Enumeration_free (e);
	    }
	  if (!bl)
	    prev = e;
	  if (bl1)
	    prev = NULL;
	  e = t;
	}
    }
  Vector_Free (V); 
  return;
}

/* Counts the number of integer points in a union of parametric poytopes.
 * This function starts by computing the disjoint union of the input
 * polytopes, then it combines Ehrhart polynomials corresponding to
 * the different (disjoints) polytopes */
 
Enumeration *Enumerate_Union (Polyhedron * P, Polyhedron * C, 
		                            const char **param_name, unsigned MaxR)
{
  Polyhedron *Q, *tmp, *Disj;
  evalue *ev;
  Enumeration *en, *res;
  int k = 0;

  ev = malloc (sizeof (evalue));
  value_init (ev->d);
  value_set_si (ev->d, 1);
  value_init (ev->x.n);
  value_set_si (ev->x.n, 0);

  res = malloc (sizeof (Enumeration));
  res->ValidityDomain = Polyhedron_Copy (C);
  res->EP = *ev;
  res->next = NULL;

  Disj = Disjoint_Domain (P, 0, MaxR);
  for (Q = Disj; Q; Q = Q->next)
    {
      k++;
      tmp = Q->next;
      Q->next = NULL;
      if (!emptyQ (Q))
	{ 
//        Polyhedron_Print(stdout, P_VALUE_FMT, Q);		  
	  en = barvinok_enumerate (Q, C, MaxR);
	  if (en)
	    {
	      Simplify_Enumeration (&en, param_name);
	      res = Enumeration_Union (res, en, MaxR);
	      Simplify_Enumeration (&res, param_name);
	    }
	  Q->next = tmp;
	}
    }
  Enumeration_fusion (&res, MaxR);
  for (en = res; en; en = en->next)
    {
      Q = en->ValidityDomain;
      en->ValidityDomain = Domain_fusion (en->ValidityDomain, MaxR);
      Domain_Free (Q);
    }
  return res;
}

/* Initializes a constant evalue */

void evalue_init (evalue * ev, int num, int den)
{
  value_init (ev->d);
  value_set_si (ev->d, den);
  if (den != 0)
    {
      value_init (ev->x.n);
      value_set_si (ev->x.n, num);
    }
}

/* Creates a fractional binary periodic number (pos mob V[0] = V[1]).
 * Such a number is required to count integer points in Z-polytopes
 * whose lattice depends on a parameter in position 'pos' */

evalue *fractionalNb (Vector * V, int pos, const char **param_name)
{
  evalue *res, *ev, *frac, *rel;
  enode *poly1, *poly2;
  int num, den;

  value_modulus (V->p[1], V->p[1], V->p[0]);
  if value_neg_p
    (V->p[1]) value_addto (V->p[1], V->p[1], V->p[0]);
  den = VALUE_TO_INT (V->p[0]);
  num = VALUE_TO_INT (V->p[1]);
  ev = malloc (sizeof (evalue));
  frac = malloc (sizeof (evalue));
  res = malloc (sizeof (evalue));
  rel = malloc (sizeof (evalue));
  evalue_init (ev, -1, 1);
  poly1 = new_enode (polynomial, 2, pos);
  evalue_set_si (&poly1->arr[1], 1, den);
  evalue_set_si (&poly1->arr[0], -num, den);
  poly2 = new_enode (polynomial, 2, pos);
  evalue_set_si (&poly2->arr[1], -1, den);
  evalue_set_si (&poly2->arr[0], num, den);
  evalue_init (res, -1, 0);
  res->x.p = new_enode (fractional, 3, -1);
  evalue_set_si (&res->x.p->arr[1], 0, 1);
  evalue_set_si (&res->x.p->arr[2], 1, 1);
  res->x.p->arr[0].x.p = poly1;
  evalue_init (frac, -1, 0);
  frac->x.p = new_enode (fractional, 3, -1);
  evalue_set_si (&frac->x.p->arr[1], 0, 1);
  evalue_set_si (&frac->x.p->arr[2], 1, 1);
  frac->x.p->arr[0].x.p = poly2;
  evalue_init (rel, -1, 0);

  rel->x.p = new_enode (relation, 2, 0);
  evalue_copy (&rel->x.p->arr[0], res);
  evalue_set_si (&rel->x.p->arr[1], 1, 1);
  value_set_si (ev->x.n, 1);
  eadd (ev, res);

  return rel;
}


int isSubIdentity (Matrix * M, unsigned n_var)
{
  int i, j;
  for (i = n_var; i < M->NbRows; i++)
    {
      for (j = n_var; j < M->NbRows; j++)
	{
	  if ((i == j && value_notone_p (M->p[i][j]))
	      || (i != j && value_notzero_p (M->p[i][j])))
	    return 0;
	}
    }
  return 1;
}

/* Organizes a lattice so that we can return to the original coordinates
 * after compression and projection of the corresponding Z-polytope */

void NormalizeLattice (Matrix ** L, unsigned nb_free_v)
{
  Matrix *M = *L, *lp, *inv, *hh, *mm;
  int i, j, n_var, vars_indep;

  n_var = M->NbRows - nb_free_v - 1;
  vars_indep = PVarsIndependent (M, n_var, nb_free_v);
  if (!vars_indep)
    {
      lp = Linear_Part (M);
      inv = InverseParamOrder (lp, nb_free_v);
      Matrix_Free (lp);
      Hermite (inv, &hh, &mm);
      Matrix_Free (inv);
      Matrix_Free (mm);
      mm = FirstParamOrder (hh, nb_free_v);
      Matrix_Free (hh);
      for (i = 0; i < mm->NbRows; i++)
	{
	  for (j = 0; j < mm->NbColumns; j++)
	    value_assign (M->p[i][j], mm->p[i][j]);
	}
      Matrix_Free (mm);
    }
}


/* Counts the number of integer points in a union of parametric Z-poytopes.
 * This function starts by applying the inclusion-exclusion principle to the 
 * input Z-polytopes, then it combines Ehrhart polynomials corresponding to
 * the resulting Z-polytopes */

Enumeration *barvinok_Zenumerate (UZPolyhedron * ZPoly, Polyhedron * C,
				                  const char **param_name, unsigned MaxR, int frac_set)
{
  SZPolyhedron *SZP, *SZPU = NULL, *RSZP, *IZP, *TZP, *TMP;
  UZPolyhedron *UZpl;
  UZPolyhedron *Zpl;
  Value v, one, nbp;
  Polyhedron *P, *Q;
  Enumeration *e, *en, *res;
  evalue *evmone, *ev, *evbn, *frac = NULL;
  Vector *V = Vector_Alloc (2);

  LatticeUnion *L;
  int i, j, k, pos;
  unsigned n_par = C->Dimension, n_var;
  Matrix *m = Matrix_Alloc (n_par + 1, n_par + 1), 
	   *h = Matrix_Alloc (n_par + 1, n_par + 2);


  if (ZPoly == NULL)
    {
      fprintf (stdout, "\n  barvinok_Zenumerate: no input Zpolytopes!!!  \n");
      ev = malloc (sizeof (evalue));
      value_init (ev->d);
      value_init (ev->x.n);
      value_set_si (ev->d, 1);
      value_set_si (ev->x.n, -1);
      en = malloc (sizeof (Enumeration));
      en->ValidityDomain = C;
      en->EP = *ev;
      return en;
    }

  value_init (v);
  value_init (one);
  value_init (nbp);
  value_set_si (one, 1);
  value_set_si (nbp, 0);

  evmone = malloc (sizeof (evalue));
  value_init (evmone->d);
  value_set_si (evmone->d, 1);
  value_init (evmone->x.n);
  value_set_si (evmone->x.n, -1);

  ev = malloc (sizeof (evalue));
  value_init (ev->d);
  value_set_si (ev->d, 1);
  value_init (ev->x.n);
  value_set_si (ev->x.n, 0);

  k = 0;
  UZpl = ZPoly;
  while (UZpl)
    {
      for (L = UZpl->LU; L; L = L->next)
	k++;
      Zpl = UZpl->next;
      UZpl->next = NULL;
      TMP = SZPolyhedron_Alloc (one, UZpl);
      TMP->next = SZPU;
      SZPU = TMP;
      UZpl = Zpl;
    }
  SZPolyhedron *pt = NULL;
  SZP = SZPU->next;
  RSZP = SZPU;
  RSZP->next = NULL;

  while (SZP)
    {
      IZP = NULL;
      for (TZP = RSZP; TZP; TZP = TZP->next)
	{
	  P = DomainIntersection (TZP->ZP->P, SZP->ZP->P, MaxR);
	 // Polyhedron_Print(stdout, P_VALUE_FMT, P);
	  if (!emptyQ (P))
	    {
	      L = LatUnionIntersection (TZP->ZP->LU, SZP->ZP->LU);
	//	Matrix_Print(stdout, P_VALUE_FMT, L->M);
	      if (L)
		{
		  value_multiply (v, TZP->sign, SZP->sign);
		  value_oppose (v, v);
		  Zpl = UZPolyhedron_Set (L, P);
		  SimplifyUZPoly (&Zpl, MaxR);
		  if (Zpl)
		    {
		      TMP = malloc (sizeof (SZPolyhedron));
		      value_init (TMP->sign);
		      value_assign (TMP->sign, v);
		      TMP->ZP = Zpl;
		      TMP->next = IZP;
		      IZP = TMP;
		    }
		}
	    }
	  else
	    {
	      Domain_Free (P);
	    }
	}
      for (TMP = RSZP; TMP->next; TMP = TMP->next);
      TMP->next = IZP;
      IZP = SZP->next;
      SZP->next = NULL;
      for (TMP = RSZP; TMP->next; TMP = TMP->next);
      TMP->next = SZP;
      SZP = IZP;
    }

  res = malloc (sizeof (Enumeration));
  res->ValidityDomain = Polyhedron_Copy (C);
  res->EP = *ev;
  res->next = NULL;
  k = 0;

  pt = NULL;

  for (TMP = RSZP; TMP; TMP = TMP->next)
    {
      int islat = 0;
      Value val;
      value_init (val);
      if (pt != NULL)
	{
	  pt->next = NULL;
	  SZDomain_Free (pt);
	}
      L = TMP->ZP->LU;
      while (L)
	{
	  k++;
	  n_var = L->M->NbRows - n_par - 1;
	  NormalizeLattice (&L->M, n_par);
	  //Matrix_Print(stdout, P_VALUE_FMT, L->M);

	  for (i = 0; i < m->NbRows; i++)
	    {
	      for (j = 0; j < m->NbColumns; j++)
		value_assign (m->p[i][j], L->M->p[i + n_var][j + n_var]);
	    }
	  for (i = 0; i < m->NbRows - 1; i++)
	    {
	      for (j = 0; j < m->NbColumns - 1; j++)
		{
		  if (i != j)
		    {
		      if (value_notzero_p (m->p[i][j]))
			{
			  Vector_Gcd (m->p[i], n_par, &val);
			  Vector_Set (m->p[i], 0, n_par);
			  value_assign (m->p[i][i], val);
			  break;
			}
		      assert (value_zero_p (m->p[i][j]));
		    }
		}
	    }
	  evbn = malloc (sizeof (evalue));
	  value_init (evbn->d);
	  value_set_si (evbn->d, 1);
	  value_init (evbn->x.n);
	  value_set_si (evbn->x.n, 1);
	  pos = 1;
	  for (i = 0; i < n_par; i++)
	    {
	      if( value_neg_p(m->p[i][i]) )
		value_oppose( m->p[i][i], m->p[i][i] );
	      if (value_notone_p (m->p[i][i]))
		{
		  islat = 1;
		  value_assign (V->p[0], m->p[i][i]);
		  value_assign (V->p[1], m->p[i][n_par]);
		  if (frac_set)
		    {
		      frac = fractionalNb (V, pos, param_name);
		      emul (frac, evbn);
		    }
		  else
		    evbn = bin_evalue (V, evbn, pos);
		}
	      pos++;
	    }
	  
	  Q = Polyhedron_Preimage (TMP->ZP->P, L->M, MaxR);
	 // Polyhedron_Print(stdout, P_VALUE_FMT, Q);
	  
	  if (!isSubIdentity (L->M, n_var))
	    {
	      for (i = 0; i < n_var; i++)
		{
		  for (j = 0; j < L->M->NbColumns; j++)
		    {
		      if (i == j)
			value_set_si (L->M->p[i][j], 1);
		      else
			value_set_si (L->M->p[i][j], 0);
		    }
		}
	      P = Polyhedron_Image (Q, L->M, MaxR);
		//Matrix_Print(stdout, P_VALUE_FMT, L->M);
		// Polyhedron_Print(stdout, P_VALUE_FMT, P);
	      Domain_Free (Q);
	      Q = P;
	    }

	  if (!emptyQ (Q))
	    {
//// affichage
/* printf("Calling barvinok:\n");
Polyhedron_Print(stdout, P_VALUE_FMT, Q);
printf("Context: ");
Polyhedron_Print(stdout, P_VALUE_FMT, C); */
////
	      en = barvinok_enumerate(Q, C, MaxR);

		if( !frac_set )
		  Enumeration_mod2table(en, C->Dimension);
 /////////// affichage  
/*           for (e = en; e; e = e->next)
	       {
	           Print_Domain (stdout, e->ValidityDomain, param_name);
	           print_evalue (stdout, &e->EP, param_name);
	           printf ("\n");
	       }
	     printf("\n**********\n");   */
/////////////////
	      Domain_Free (Q);
	      if (en)
		{
		  if (value_notzero_p (en->EP.d) && value_mone_p (en->EP.x.n))
		    {
		      if (frac_set)
			  free_evalue_refs (frac);
		      free_evalue_refs (evbn);
		      Enumeration_free (res);
		      Matrix_Free (m);
		      Matrix_Free (h);
		      value_clear (v);
		      value_clear (one);
		      value_clear (nbp);
		      return en;
		    }

		  if (value_neg_p (TMP->sign))
		    {
		      for (e = en; e; e = e->next)
			emul (evmone, &e->EP);
		    }
		  if (islat)
		    {
		      for (e = en; e; e = e->next)
			{
// emul does not know how to multiply a periodic by a nonperiodic number... e->EP has to be converted.
			    if( !(value_zero_p(e->EP.d) && e->EP.x.p->type==periodic)
			         && value_zero_p(evbn->d) && evbn->x.p->type == periodic )
				e->EP = evalue_makeperiodic( e->EP, evbn->x.p->pos );
//// affichage
/*	print_evalue (stdout, evbn, param_name); printf("\n");
	print_evalue (stdout, &e->EP, param_name);printf("\n");  */
////
			  emul (evbn, &e->EP);
			}
		      if (frac_set)
			free_evalue_refs (frac);
		      free_evalue_refs (evbn);
		    }
/////////// affichage 
         /*  for (e = en; e; e = e->next)
	       {
	           Print_Domain (stdout, e->ValidityDomain, param_name);
	           print_evalue (stdout, &e->EP, param_name);
	           printf ("\n");
	       }
	     printf("\n**********\n");   */
/////////////////
	        e=res;
		  res = Enumeration_Union (en, res, MaxR);
		  Simplify_Enumeration (&res, param_name);
		  Enumeration_fusion (&res, MaxR);
		  Enumeration_free (e);
		  Enumeration_free (en);
		}
	    }
	  L = L->next;
	}
      pt = TMP;
      value_clear (val);
    }

  for (en = res; en; en = en->next)
    {
      P = en->ValidityDomain;
      en->ValidityDomain = Domain_fusion (en->ValidityDomain, MaxR);
      Domain_Free (P);
    }
  Matrix_Free (m);
  Matrix_Free (h);
  value_clear (v);
  value_clear (one);
  value_clear (nbp);
  return res;
}

static evalue evalue_makeperiodic( evalue e, int p )
{
	evalue epp;
	value_init (epp.d);
	value_set_si (epp.d, 0);
	epp.x.p = malloc( sizeof(enode) );
	epp.x.p->type = periodic;
	epp.x.p->size = 1;
	epp.x.p->pos = p;
	epp.x.p->arr[0] = e;
	return( epp );

}

int Read_INT (void)
{
  char *c, s[1024], str[1024];
  int n;
  int u;
  Value v;
  value_init (v);
  do
    {
      c = fgets (s, 1024, stdin);
      while (c && isspace (*c) && *c != '\n')
        ++c;
    }
  while (c && (*c == '#' || *c == '\n'));
  if (!c || *c == '\n' || *c == '#')
    {
      errormsg1 ("Read_NbExist", "badNb_Exist",
                 "Bad number of existential variables");
      return -1;
    }
  sscanf (c, "%s%n", str, &n);
  value_read (v, str);
  u = VALUE_TO_INT (v);
  value_clear (v);
  return u;
}

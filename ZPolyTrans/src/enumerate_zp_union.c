/*
 * This file is part of ZPolyTrans.

    ZPolyTrans is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ZPolyTrans is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ZPolyTrans.  If not, see <http://www.gnu.org/licenses/>.
 */
/* Written by Rachid Seghir, 2005-2010 */
/* Modified by Vincent Loechner, 2011 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <assert.h>
#include <sys/times.h>
#include <barvinok/barvinok.h>
#include <polylib/polylibgmp.h>
#include "Zenumerate.h"
#include <stdlib.h>
#include <ctype.h>

#define MAXRAYS 0

#if __STDC_VERSION__ < 199901L
# if __GNUC__ >= 2
#  define __func__ __FUNCTION__
# else
#  define __func__ "<unknown>"
# endif
#endif
#define ERREUR(x) do { fprintf(stderr, "%s (%s:%d): %s\n", __func__, __FILE__, __LINE__, x); exit(1); } while(0)
#undef  PRINT_PERF


int main (int argc, char **argv)
{

  Matrix *M;
  Lattice *L;
  LatticeUnion *LU;
  Polyhedron *P, *C;
  Enumeration *e, *en;
  Value one;
  const char **param_name;
  int i, NbZP;
  UZPolyhedron *SZPU, *SZP;
#ifdef PRINT_PERF
  struct tms  t1,t2;
  long ticks = sysconf (_SC_CLK_TCK);
#endif
  int config_print_result=0, config_debug=0, config_frac=1;

  for( i=1; i<argc ; i++ )
  {
	if( !strncmp( "-h", argv[i], 2 ) || !strncmp( "--help", argv[i], 6 ) )
	{
		printf( "Usage: %s [-h] [-v] [-d]\n", argv[0] );
		printf( "  -h: this help message\n" );
		printf( "  -v: verbose, prints union of Z-polytopes\n" );
		printf( "  -d: debug\n" );
		printf( "  -periodic: use periodic numbers in Ehrhart polynomials (default: fractional)\n" );
		exit(0);
	}
	else if( !strncmp( "-v", argv[i], 2 ) )
	{
		config_print_result = 1;
	}
	else if( !strncmp( "-d", argv[i], 2 ) )
	{
		config_debug = 1;
	}
	else if( !strncmp( "-periodic", argv[i], 9 ) )
	{
		config_frac = 0;
	}
	else
	{
		printf("Ignoring unrecognized option %s\n", argv[i] );
	}
  }


  if( config_debug )
	printf("reading number of input polyhedra\n");

  NbZP = Read_NbPoly ();
  if( NbZP<=0 )
	ERREUR("NbZP: Invalid input\n");

#ifdef PRINT_PERF
  times(&t1);  
#endif

  value_init (one);
  value_set_si (one, 1);
  SZPU = NULL;
  for (i = 0; i < NbZP; i++)
    {
	  if( config_debug )
	    printf("reading polyhedron %d\n",i+1);
      M = Matrix_Read ();
      P = Constraints2Polyhedron (M, MAXRAYS);
	  Matrix_Free (M);
	  if( !P )
	    ERREUR("Invalid Input Polyhedron");

	  if( config_debug )
	    printf("reading transformation function %d\n",i+1);
      L = Matrix_Read ();

	  if( config_debug )
	  {
	    printf("Computing the transformation of P:\n");
	    Polyhedron_Print(stdout, P_VALUE_FMT, P);
	    printf("by function L:\n");
	    Matrix_Print(stdout, P_VALUE_FMT, L);
	  }
      LU = LatticeUnion_Alloc ();
      LU->M = L;
      SZP = UZPolyhedron_Set (LU, P);
      SZP->next = SZPU;
      SZPU = SZP;
    }

  if( config_debug )
    printf("Reading Context\n");
  M = Matrix_Read ();
  C = Constraints2Polyhedron (M, MAXRAYS);
  Matrix_Free (M);
  if( !C )
    ERREUR("Context: Invalid Input Polyhedron");

  param_name = Read_ParamNames (stdin, C->Dimension);

  if( config_print_result )
    UZDomain_Print(SZPU);

  if( config_debug )
    printf("Enumerating\n");
  en = barvinok_Zenumerate (SZPU, C, param_name, MAXRAYS, config_frac);

  for (e = en; e; e = e->next)
  {
    Print_Domain (stdout, e->ValidityDomain, param_name);
    print_evalue (stdout, &e->EP, param_name);
    printf ("\n");
  }

  if( config_debug )
    printf("Freeing memory\n");

  UZDomain_Free (SZPU);
  Free_ParamNames (param_name, C->Dimension);
  Polyhedron_Free (C);
  value_clear (one);

#ifdef PRINT_PERF
  times(&t2);
  printf("\n\nExecution time: ");
  printf ("usr: %g seconds  sys: %g  seconds \n",
	  (0.0 + t2.tms_utime - t1.tms_utime) / ticks,
	  (0.0 + t2.tms_stime - t1.tms_stime) / ticks);
#endif

  return 0;
}				/* main */

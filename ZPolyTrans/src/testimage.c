# include <stdio.h>
# include <polylib/polylibgmp.h>
# include <piplib/piplibMP.h>
# include <barvinok/evalue.h>
# include <barvinok/barvinok.h>
# include "Zenumerate.h"
#include <omp.h>
# include "transform.h"

#define MAXRAYS 0
#define MAXPAR 5
#define ERREUR(x) do { fprintf(stderr, "%s (%s:%d): %s\n", __func__, __FILE__, __LINE__, x); exit(1); } while(0)

int failed = 0;

int config_print_result=0; /* verbose */
PipMatrix * domain; 
PipOptions * options ;
int dim_pipctx, hdim, npar, Nb_solution=0;
Polyhedron *D, *DP;
Enumeration *en=NULL, *en1=NULL;

int min_lex(Value *context){
  int sol=1, i, neg=0;
  PipQuast * solution ;
  PipMatrix *pipctx=pip_matrix_alloc(dim_pipctx, dim_pipctx+2);
  options->Urs_parms=0;

  for(i=0; i<dim_pipctx;i++){
    value_set_si(pipctx->p[i][0], 0);
    value_set_si(pipctx->p[i][i+1], 1);
    value_assign(pipctx->p[i][dim_pipctx+1], context[i+1]);
    if(value_neg_p(context[i+1])) neg=1;
    value_oppose(pipctx->p[i][dim_pipctx+1], pipctx->p[i][dim_pipctx+1]);
  }
  if(neg) options->Urs_parms=-1;
  solution = pip_solve(domain,pipctx,-1,options); 
  if(!solution||
     (!solution->list && !solution->next_then && !solution->next_else))sol=0;
  pip_quast_free(solution);
  pip_matrix_free(pipctx);
  return sol;
}

void Scan(int pos, Polyhedron *P, Value *context){
  Value LB, UB, k;
    
  value_init(LB); value_init(UB);value_init(k);
  value_set_si(LB,0);
  value_set_si(UB,0);

  if (lower_upper_bounds(pos,P,context,&LB,&UB) !=0) {
    /* Problem if UB or LB is INFINITY */
    fprintf(stderr, "count_points: ? infinite domain\n");
    value_clear(LB); value_clear(UB); value_clear(k);
    return;
  }
  if(!P->next){
    for (value_assign(k,LB);value_le(k,UB); value_increment(k,k)){	
      value_assign(context[pos],k);
      Nb_solution+=min_lex(context);		  
    }	 
    value_set_si(context[pos],0);
    value_clear(LB); value_clear(UB); value_clear(k);
    return ;
  }  
    
  value_set_si(context[pos],0);
    
  for (value_assign(k,LB);value_le(k,UB); value_increment(k,k)) {
    value_assign(context[pos],k);
    Scan(pos+1, P->next, context);
  }
  value_set_si(context[pos],0);
  value_clear(LB); value_clear(UB); value_clear(k);
  return;
}


Value *min_vertex(Enumeration *e){
  int min, i, j;  
  double vmin, val; 
  Polyhedron *T;

  Value *vec=malloc(npar*sizeof(Value));
  for(j=0;j<npar;j++) value_init(vec[j]);
  T=e->ValidityDomain;  		
  for(i=0; i< T->NbRays;i++)
    if(value_notzero_p(T->Ray[i][npar+1])) break;
  min=i;
  if(value_notone_p(T->Ray[i][npar+1]))
    for(j=0;j<npar;j++)
      value_division(vec[j], T->Ray[i][j+1], T->Ray[i][npar+1]);
  else
    for(j=0;j<npar;j++) value_assign(vec[j], T->Ray[i][j+1]);

  if (i!=T->NbRays-1) {
    val=vmin=compute_evalue(&e->EP, vec);
    //printf("Value = %f   vertex %d\n\n",val, i);
    for(i=i+1; i<T->NbRays; i++) {
      if(value_notzero_p(T->Ray[i][npar+1])){
	if(value_notone_p(T->Ray[i][npar+1]))
	  for(j=0;j<npar;j++)	   
	    value_division(vec[j], T->Ray[i][j+1], T->Ray[i][npar+1]);
	else
	  for(j=0;j<npar;j++) value_assign(vec[j], T->Ray[i][j+1]);	  
	val=compute_evalue(&e->EP, vec);
	if(val < vmin) {
	  vmin=val;	
	  min=i;   
	} 
      }
    }
  }
       
  if(value_notone_p(T->Ray[min][npar+1]))
    for(j=0;j<npar;j++)
      value_division(vec[j], T->Ray[min][j+1], T->Ray[min][npar+1]);
  else
    for(j=0;j<npar;j++) value_assign(vec[j], T->Ray[min][j+1]);

  return vec;
}

void check( Value *contextp, Value *context )
{    
  int i, j;
  Value *val;

  val=compute_poly(en, contextp+1);
  if( config_print_result )
    {
      Value *val1;
      printf("\nParameters are fixed to :");
      for(i=0;i<npar;i++){
	value_print(stdout, P_VALUE_FMT, contextp[i+1]);
      }
      printf("\n");

      val1=compute_poly(en1, contextp+1);
      printf("Number of solutions in the rational image %d\n",VALUE_TO_INT(*val1));
      value_clear(*val1);
      printf("Value of EP for the fixed parameters %d\n",VALUE_TO_INT(*val));
    }

  j=1;
  for(i=hdim-npar; i<hdim; i++) 
    value_assign(context[i],contextp[j++]);

  Nb_solution=0;
  Scan(1, D, context);

  if(Nb_solution != VALUE_TO_INT(*val))
    failed = -1;

  if( config_print_result )
    {
      printf("Number of solutions with PIP : %d\n", Nb_solution);
    }
  else
    {
      if(Nb_solution == VALUE_TO_INT(*val))
	printf("O");
      else
	printf("X");
      fflush(stdout);
    }
  value_clear(*val);
}



void Scan_Param(int pos, Polyhedron *P, Value *contextp, Value *context){
  Value LB, UB, k;
  double begin, end;
  value_init(LB); value_init(UB);value_init(k);
  value_set_si(LB,0);
  value_set_si(UB,0);

  if (lower_upper_bounds(pos,P,contextp,&LB,&UB) !=0) {
    /* Problem if UB or LB is INFINITY */
    fprintf(stderr, "count_points: ? infinite domain\n");
    value_clear(LB); value_clear(UB); value_clear(k);
    return;
  }
  if(!P->next){
    for (value_assign(k,LB);value_le(k,UB); value_increment(k,k)){	
      value_assign(contextp[pos],k);
      check(contextp, context);
    }	 
    value_set_si(contextp[pos],0);
    value_clear(LB); value_clear(UB); value_clear(k);
    return ;
  }  
    
  value_set_si(contextp[pos],0);

  for (value_assign(k,LB);value_le(k,UB); value_increment(k,k)) {
    value_assign(contextp[pos],k);
    Scan_Param(pos+1, P->next, contextp, context);
  }

  value_set_si(contextp[pos],0);
  value_clear(LB); value_clear(UB); value_clear(k);
  return;
}


int main(int argc, char **argv)
{       
  int nbpoly, nexi, i, j, single=0;
  Polyhedron *P, *C,*Q, *T;
  UZPolyhedron *ZPU = NULL;
  Matrix *M, *MT;
  const char **param_name;
  Value *context; 
  Enumeration *e; 
  evalue *ev;
  int MaxPar=MAXPAR;

  double begin, end;

  int config_computeEP=1, config_debug=0, config_frac=1; 	  
  for( i=1; i<argc ; i++ )
    {
      if( !strncmp( "-h", argv[i], 2 ) || !strncmp( "--help", argv[i], 6 ) )
	{
	  printf( "Usage: %s [-h] [-v] [-d] [-no-ep] [r <num>]\n", argv[0] );
	  printf( "  -h: this help message\n" );
	  printf( "  -v: verbose, print resulting union of Z-polytopes\n" );
	  printf( "  -d: debug\n" );
	  printf( "  -no-ep: don't compute Ehrhart polynomial of the result (implies -v)\n" );
	  printf( "  -periodic: use periodic numbers in Ehrhart polynomials (default: fractional)\n" );
	  printf( "  -r <num>: range of parameter values to test (default: %d)\n",  MAXPAR );
	  exit(0);
	}
      else if( !strncmp( "-r", argv[i], 2 ) )
	{
	  i++;
	  if( i>=argc )
	    printf("Missing value for option -m.\n");
	  else
	    MaxPar = atoi(argv[i]);
	}
      else if( !strncmp( "-v", argv[i], 2 ) )
	{
	  config_print_result = 1;
	}
      else if( !strncmp( "-d", argv[i], 2 ) )
	{
	  config_debug = 1;
	}
      else if( !strncmp( "-no-ep", argv[i], 6 ) )
	{
	  config_computeEP = 0;
	  config_print_result = 1;
	}
      else if( !strncmp( "-periodic", argv[i], 9 ) )
	{
	  config_frac = 0;
	}
      else
	{
	  printf("Ignoring unrecognized option %s\n", argv[i] );
 	}
    }

  nbpoly=Read_INT();
  if(nbpoly!=1)
    ERREUR("TestImage only tests the image of a single polyhedron\n");
  M=Matrix_Read();
  MT=Matrix_Copy(M);
  P=Constraints2Polyhedron(M, MAXRAYS);

  if( !P )
    ERREUR("Invalid Input Polyhedron");	    
	  
  Matrix_Free(M);
  nexi=Read_INT();
  M=Matrix_Read();
  C=Constraints2Polyhedron(M, MAXRAYS);
  param_name = Read_ParamNames(stdin, C->Dimension);
  if( !C )
    ERREUR("Invalid Input Polyhedron");
  Matrix_Free(M);
           
  npar=C->Dimension;
  hdim=P->Dimension+1;
  dim_pipctx=hdim-nexi-1;
  options = pip_options_init();
  options->Urs_unknowns=-1;		    

  M = Matrix_Alloc(hdim, hdim);
  domain = pip_matrix_alloc(P->NbConstraints, hdim+1); 
  for(i=0+nexi; i<hdim; i++)
    value_set_si(M->p[i][i], 1);
  for(i=0; i<domain->NbRows; i++)
    for(j=0; j< domain->NbColumns; j++)
      value_assign(domain->p[i][j], P->Constraint[i][j]);	   

  Q=Polyhedron_Image(P, M, MAXRAYS);
  Matrix_Free(M);
  M=Matrix_Alloc(Q->NbConstraints-nexi,hdim-nexi+1);
  for(i=0; i<Q->NbConstraints-nexi; i++){
    value_assign(M->p[i][0], Q->Constraint[i+nexi][0]);
    for(j=1; j<=hdim-nexi; j++)
      value_assign(M->p[i][j],Q->Constraint[i+nexi][j+nexi]);
  }
  Polyhedron_Free(Q);
  Q=Constraints2Polyhedron(M, MAXRAYS);
  Matrix_Free(M);
  en1 =barvinok_enumerate (Q, C, MAXRAYS);
  D=Polyhedron_Scan(Q, C, 200);
  Polyhedron_Free(Q);
  hdim=D->Dimension+1;
  context = malloc((hdim+1)*sizeof(Value));
  for (j=0;j<= hdim;j++)
    value_init(context[j]);
  Vector_Set(context,0,(hdim+1));
  value_set_si(context[hdim],1);
      
  Polyhedron_Free(P);
  PreChooseOrder (&MT, nexi);
  P = Constraints2Polyhedron (MT, MAXRAYS);
  if( !P )
    ERREUR("Invalid Input Polyhedron");
  Matrix_Free (MT);
  if (!config_print_result )
    {
      if (nexi - P->NbEq == 1)
	{
	  if (NbTests (P) > 20)
	    {
              if( config_debug )
		printf("Single=1, Projecting USING_ONE_DIM\n");
	      single = 1;
	      Q = ProjectOneDim (P, nexi);
	    }
	}
    }

  if (!single)
    {

      ZPU = Project_Polytope (P, C, nexi, 1, !config_print_result);
   

      if( config_debug )
	{
	  printf("Result of projection:\n");
	  UZDomain_Print(ZPU);
        }

      if (ZPU == NULL)
	{
	  if( config_print_result )
	    printf("Empty\n");

	  if( config_computeEP )
	    {
	      ev = malloc (sizeof (evalue));
	      value_init (ev->d);
	      value_init (ev->x.n);
	      value_set_si (ev->d, 1);
	      value_set_si (ev->x.n, 0);
	      en = malloc (sizeof (Enumeration));
	      en->ValidityDomain = Polyhedron_Copy (C);
	      en->EP = *ev;
	      en->next = NULL;
	    }
	}
      else
	{
	  if( config_print_result )
	    {
	      UZDomain_Print(ZPU);
	      printf("\n");
	    }
	  if( config_computeEP )
	    {
	      if( config_debug )
		printf("Enumerating\n");
	      en = barvinok_Zenumerate (ZPU, C, param_name, MAXRAYS, config_frac);
	    }
	}
    }
  else //single = 1, config_print_result = 0, USING_ONE_DIM
    {
      if( config_debug )
	printf("single and using_one_dim\n");

      if(config_computeEP)
	{
	  if( config_debug )
	    printf("Enumerating\n");
	  en = Enumerate_Union (Q, C, param_name, MAXRAYS);
	}
      Polyhedron_Free (Q);
    }

  if(config_computeEP && config_print_result )
    {
      printf("\nNumber of solutions with enumerate_image \n\n");	    
      for (e = en; e; e = e->next)
        {
          Print_Domain (stdout, e->ValidityDomain, param_name);
          print_evalue (stdout, &e->EP, param_name);
          if (e->next)
            printf ("\n");
        }
    }
   
  Polyhedron *CP;
  M=Matrix_Alloc(0,2);
 
  CP=Constraints2Polyhedron(M,MAXRAYS);
 
Matrix_Free(M);

  M=Matrix_Alloc(2*npar, npar+2);
  for(i=0; i<npar; i++){
    value_set_si(M->p[i][0], 1);
    value_set_si(M->p[i+npar][0], 1);
    value_set_si(M->p[i][i+1], 1);
    value_set_si(M->p[i+npar][i+1],-1);
  }
    
  Value *contextp = malloc((npar+2)*sizeof(Value));
  for (j=0;j<= npar+1;j++)
    value_init(contextp[j]);
  Vector_Set(contextp,0,(npar+2));
  value_set_si(contextp[npar+1],1);

  Value *vert=NULL;
  for (e = en1; e; e = e->next) {
    for(i=0; i<npar; i++) {
      value_set_si(M->p[i][npar+1], MaxPar);
      value_set_si(M->p[i+npar][npar+1], MaxPar);
    }

    vert=min_vertex(e);
    for(i=0; i<npar; i++){
      value_subtract(M->p[i][npar+1], M->p[i][npar+1], vert[i]);
      value_addto(M->p[i+npar][npar+1], M->p[i+npar][npar+1], vert[i]);
    }
 
    Q=Constraints2Polyhedron(M,MAXRAYS);

    T=DomainIntersection(e->ValidityDomain, Q, MAXRAYS);

    DP=Polyhedron_Scan(T, CP, 200);

    Scan_Param(1, DP, contextp, context);
   

    Polyhedron_Free(Q);
    Polyhedron_Free(T);
    Polyhedron_Free(DP);
  }
  printf("\n");

  if( config_debug )
    printf("Freeing memory\n");

  if(vert)
    for(j=0;j<npar;j++) value_clear(vert[j]);
  for(i=0; i<=hdim; i++)
    value_clear(context[i]);
  for (j=0;j<= npar+1;j++)
    value_clear(contextp[j]);
  Matrix_Free(M);
  Polyhedron_Free (C);
  Polyhedron_Free (CP);
  Polyhedron_Free (P);
  Polyhedron_Free (D);
  UZDomain_Free (ZPU);
  if(en)
    Enumeration_free (en);
  pip_options_free(options) ;
  pip_matrix_free(domain) ;
  pip_close() ;
  
  if( failed )
    fprintf(stderr, "******* CHECK FAILED! *******\n" );
  exit( failed );
}


/*
 * This file is part of ZPolyTrans.

 ZPolyTrans is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 ZPolyTrans is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with ZPolyTrans.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Written by Rachid Seghir, 2005-2010 */
/* Modified by Vincent Loechner, 2011 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stddef.h>
#include <sys/times.h>
#include <math.h>
#include <polylib/polylibgmp.h>
#include <barvinok/barvinok.h>
#include <omp.h>
#include "Zenumerate.h"
#include "transform.h"

static Polyhedron * Polyhedron_Trancate2(Polyhedron * P, unsigned MaxRays); 


#define MAXRAYS   0

//#define USING_ONE_DIM

int sum_coef (Polyhedron * P, int n_exi)
{
  unsigned int i;
  int j;
  Value v, val;
  value_init (v);
  value_init (val);
  value_set_si (v, 0);
  for (i = 0; i < P->NbConstraints; i++)
    {
      for (j = 1; j <= n_exi; j++)
	{
          value_absolute (val, P->Constraint[i][j]);
          value_addto (v, v, val);
	}
    }
  i = VALUE_TO_INT (v);
  value_clear (val);
  value_clear (v);
  return i;
}

static void Ceil (Value * c, Value * num, Value * den)
{
  Value tmp, cei;
  value_init (tmp);
  value_init (cei);
  value_oppose (tmp, *num);
  value_modulus (cei, tmp, *den);
  if (value_neg_p (cei))
    value_addto (cei, cei, *den);
  value_addto (cei, cei, *num);
  value_division (*c, cei, *den);
  value_clear (tmp);
  value_clear (cei);
}

static void Floor (Value * c, Value * num, Value * den)
{
  Value cei;
  value_init (cei);

  value_modulus (cei, *num, *den);
  if (value_neg_p (cei))
    value_addto (cei, cei, *den);
  value_substract (cei, *num, cei);
  value_division (*c, cei, *den);

  value_clear (cei);
}

/* Computes the exact shadow (rational projection) corresponding to 
 * a pair of lower and upper bounds on an existential variable    */

Polyhedron *Exact_Shadow (Vector * V1, Vector * V2, Value * alp, Value * bet,
			  Matrix ** M, unsigned nwdim)
{
  Polyhedron *pol;
  unsigned k, univer = 1;

  Matrix *MT1 = Matrix_Alloc (1, nwdim + 1), 
    *MT2 = Matrix_Alloc (1, nwdim + 1), *MM;
  *M = Matrix_Alloc (1, nwdim + 2);
  value_set_si ((*M)->p[0][0], 1);
  Vector_Scale (V2->p, MT1->p[0], *alp, nwdim + 1);
  Vector_Scale (V1->p, MT2->p[0], *bet, nwdim + 1);
  if (value_pos_p (*alp))
    {
      Vector_Sub (MT1->p[0], MT2->p[0], (*M)->p[0] + 1, nwdim + 1);
    }
  else
    {
      Vector_Sub (MT2->p[0], MT1->p[0], (*M)->p[0] + 1, nwdim + 1);
    }
  MM = Matrix_Copy (*M);
  for (k = 1; k <= nwdim; k++)
    {
      if (value_notzero_p (MM->p[0][k]))
	{
	  univer = 0;
	  break;
	}
    }

  if (univer)
    {
      pol = Universe_Polyhedron (nwdim);
    }
  else
    {
      pol = Constraints2Polyhedron (MM, MAXRAYS);
    }

  Matrix_Free (MT1);
  Matrix_Free (MT2);
  Matrix_Free (MM);

  return (pol);
}

/* Computes the dark shadow (convex region of the integer projection) 
 * corresponding to a pair of lower and upper bounds on an existential 
 * variable */

Polyhedron *Dark_Shadow (Vector * V1, Vector * V2, Value * alp, Value * bet, unsigned nwdim)
{
  Polyhedron *p;
  Value tmp, tmp1, tmp2;
  unsigned dim = nwdim;
  Matrix *M;

  Exact_Shadow (V1, V2, alp, bet, &M, nwdim);

  value_init (tmp);
  value_init (tmp1);
  value_init (tmp2);
  value_absolute (tmp1, *alp);
  value_absolute (tmp2, *bet);
  value_multiply (tmp, tmp1, tmp2);
  value_substract (tmp, tmp, tmp1);
  value_substract (tmp, tmp, tmp2);
  value_increment (tmp, tmp);

  value_substract (M->p[0][dim + 1], M->p[0][dim + 1], tmp);
  p = Constraints2Polyhedron (M, MAXRAYS);
  Matrix_Free (M);
  value_clear (tmp1);
  value_clear (tmp2);
  return p;
}

/* Computes the integer projection of a pair of bounds when the lower/upper 
 * bound is a constant (independent of the non-existential variables) */

Polyhedron *Const_Shadow (Vector * V1, Vector * V2, Value * alp, Value * bet, unsigned dim)
{
  Polyhedron *p;
  Value tmp, opp;
  Matrix *M = Matrix_Alloc (1, dim + 2);
  unsigned i, univer = 1;

  value_init (tmp);
  value_init (opp);
  for (i = 0; i < dim; i++)
    {
      if (value_notzero_p (V2->p[i]))
	{
	  univer = 0;
	  break;
	}
    }
  if (univer)
    {
      p = Universe_Polyhedron (dim);
    }
  else
    {
      value_set_si (M->p[0][0], 1);
      for (i = 0; i <= dim; i++)
	{
	  value_assign (M->p[0][i + 1], V2->p[i]);
	}
      if (value_pos_p (*alp))
	{
	  value_oppose (opp, V1->p[dim]);
	  Ceil (&tmp, &opp, alp);
	  value_multiply (tmp, tmp, *bet);
	  value_addto (M->p[0][dim + 1], M->p[0][dim + 1], tmp);
	}
      else
	{
	  value_oppose (opp, *alp);
	  Floor (&tmp, &V1->p[dim], &opp);
	  value_multiply (tmp, tmp, *bet);
	  value_addto (M->p[0][dim + 1], M->p[0][dim + 1], tmp);
	}
      p = Constraints2Polyhedron (M, MAXRAYS);
    }
  Matrix_Free (M);
  value_clear (tmp);
  return p;
}

/* Checks whether a hyperplane (in function Split_Coprime) is a hole 
 * for the given values of constants */ 

int hole (Value * alpha, Value * beta, Value * c1, int gamma)
{
  Value tmp, vgamma, absalph, absbet;
  
  value_init (absbet);
  value_init (absalph);
  value_init (tmp);
  value_init (vgamma);
  value_absolute (absbet, *beta);
  value_absolute (absalph, *alpha);
  value_set_si (vgamma, gamma);
  value_multiply (tmp, *c1, vgamma);
  value_modulus (tmp, tmp, absbet);
  if (value_neg_p (tmp))
    {
      value_addto (tmp, tmp, absbet);
    }
  value_multiply (tmp, tmp, absalph);
  value_clear (absbet);
  value_clear (absalph);
  if (value_gt (tmp, vgamma))
    { 
      value_clear (tmp);
      value_clear (vgamma);	
      return 1;
    }
  else
    {
      value_clear (tmp);
      value_clear (vgamma);
      return 0;
    }
}

/* When the coefficents of the existential variable are coprime, this function 
 * computes the hyperplanes containing the points of the integer projection.
 * Only integer points which are outside the dark shadow are taken into account */

Polyhedron *Split_Coprime (Polyhedron * P, Value * alpha, 
			   Value * beta,  Value * c1, unsigned NbEx)
{
  Polyhedron *Q, *D = NULL;
  Value ab_alp, ab_bet, tmp, gcd, mod;
  unsigned dim = P->Dimension, di;
  Matrix *MC, *M = Matrix_Alloc (1, dim + 2), *MM = Matrix_Alloc (2, dim + 2);
  int i, cte, gamma;

  value_init (ab_alp);
  value_init (ab_bet);
  value_init (tmp);
  value_init (gcd);
  value_init (mod);

  value_absolute (ab_alp, *alpha);
  value_absolute (ab_bet, *beta);
  value_multiply (tmp, ab_alp, ab_bet);
  value_subtract (tmp, tmp, ab_alp);
  value_subtract (tmp, tmp, ab_bet);
  cte = VALUE_TO_INT (tmp);

  value_set_si (M->p[0][0], 0);
  if (NbEx == 1)
    {
      value_set_si (MM->p[0][0], 1);
      value_set_si (MM->p[1][0], 1);
    }
  for (di = 1; di <= dim; di++)
    {
      value_assign (M->p[0][di], P->Constraint[0][di]);
      if (NbEx == 1)
	{
	  value_assign (MM->p[0][di], P->Constraint[0][di]);
	  value_oppose (MM->p[1][di], P->Constraint[0][di]);
	}
    }
  value_addto (M->p[0][dim + 1], P->Constraint[0][dim + 1], tmp);
  value_increment (M->p[0][dim + 1], M->p[0][dim + 1]);
  if (NbEx == 1)
    {
      value_addto (MM->p[0][dim + 1], P->Constraint[0][dim + 1], tmp);
      value_increment (MM->p[0][dim + 1], MM->p[0][dim + 1]);
      value_oppose (MM->p[1][dim + 1], MM->p[0][dim + 1]);
    }

  Vector_Gcd (M->p[0] + 1, dim, &gcd);
  int j = 0, *vi = (int *)malloc (cte * sizeof (int));

  for (gamma = 0; gamma <= cte; gamma++)
    {
      value_set_si (tmp, -gamma);
      value_addto (tmp, tmp, M->p[0][dim + 1]);
      value_modulus (mod, tmp, gcd);
      if (value_zero_p (mod))
	break;
    }
  for ( ; gamma <= cte; gamma+=VALUE_TO_INT(gcd) )
    
    {
      if (!hole (alpha, beta, c1, gamma))
	{
	  vi[j] = gamma;	//
	  if (NbEx != 1)
	    {
	      value_set_si (tmp, -vi[j]);	  
	      MC = Matrix_Copy (M);
	      value_addto (MC->p[0][dim + 1], MC->p[0][dim + 1], tmp); 
	      Q = Constraints2Polyhedron (MC, MAXRAYS);
	      Matrix_Free (MC);
	      Q->next = D;
	      D = Q;
	    }
	  j++;
	}
    }
  // vi[j] = 0;

  if (NbEx == 1)
    {
      i = 0;
      unsigned k;
      while (i < j)
	{
	  if (i == j - 1)
	    {
	      value_set_si (tmp, -vi[i]);
	      MC = Matrix_Copy (M);
	      value_addto (MC->p[0][dim + 1], MC->p[0][dim + 1], tmp);
	      Q = Constraints2Polyhedron (MC, MAXRAYS);
	      Matrix_Free (MC);
	      i++;
	      Q->next = D;
	      D = Q;
	    }
	  else
	    {
	      if (vi[i] != vi[i + 1] - 1)
		{
		  value_set_si (tmp, -vi[i]);
		  MC = Matrix_Copy (M);
		  value_addto (MC->p[0][dim + 1], MC->p[0][dim + 1], tmp);
		  Q = Constraints2Polyhedron (MC, MAXRAYS);
		  Matrix_Free (MC);
		  i++;
		  Q->next = D;
		  D = Q;
		}
	      else
		{
		  k = i;
		  while (i < j - 1 && vi[i] == vi[i + 1] - 1)
		    i++;

		  value_set_si (tmp, -vi[k]);
		  MC = Matrix_Copy (MM);
		  value_addto (MC->p[0][dim + 1], MC->p[0][dim + 1], tmp);
		  value_set_si (tmp, vi[i]);
		  value_addto (MC->p[1][dim + 1], MC->p[1][dim + 1], tmp);
		  Q = Constraints2Polyhedron (MC, MAXRAYS);
		  Matrix_Free (MC);
		  i++;
		  Q->next = D;
		  D = Q;
		}
	    }
	}
    }

  Matrix_Free (M);
  Matrix_Free (MM);
  value_clear (ab_alp);
  value_clear (ab_bet);
  value_clear (tmp);
  value_clear (gcd);
  value_clear (mod);
  return D;
}

void UpTriangurLattice (Matrix ** M)
{
  Matrix *m, *U, *H;
  unsigned i, j, bl = 1;
  
  for (i = 0; i < (*M)->NbRows && bl; i++)
    for (j = 0; j < i; j++)
      if (value_notzero_p ((*M)->p[i][j]))
	bl = 0;
  if (bl)
    return;

  m = Matrix_Alloc ((*M)->NbRows - 1, (*M)->NbColumns - 1);
  for (i = 0; i < m->NbRows; i++)
    for (j = 0; j < m->NbColumns; j++)
      value_assign (m->p[i][j], (*M)->p[i][j]);
  U = Matrix_Alloc (m->NbRows, m->NbColumns);
  for (i = 0; i < m->NbRows - 1; i++)
    for (j = 0; j < m->NbColumns; j++)
      value_assign (U->p[i][j], m->p[i + 1][j]);
  for (j = 0; j < m->NbColumns; j++)
    value_assign (U->p[i][j], m->p[0][j]);
  for (i = 0; i < U->NbRows; i++)
    value_assign (m->p[i][U->NbColumns - 1], U->p[i][0]);
  for (i = 0; i < U->NbRows; i++)
    for (j = 1; j < U->NbColumns; j++)
      value_assign (m->p[i][j - 1], U->p[i][j]);
  
  Matrix_Free (U);
  Hermite (m, &H, &U);
  for (i = 0; i < H->NbColumns; i++)
    value_assign (m->p[0][i], H->p[H->NbRows - 1][i]);

  for (i = 1; i < H->NbRows; i++)
    for (j = 0; j < H->NbColumns; j++)
      value_assign (m->p[i][j], H->p[i - 1][j]);

  for (i = 0; i < m->NbRows; i++)
    value_assign (U->p[i][0], m->p[i][m->NbColumns - 1]);

  for (i = 0; i < m->NbRows; i++)
    for (j = 1; j < m->NbColumns; j++)
      value_assign (U->p[i][j], m->p[i][j - 1]);

  for (i = 0; i < U->NbRows; i++)
    for (j = 0; j < U->NbColumns; j++)
      value_assign ((*M)->p[i][j], U->p[i][j]);
  Matrix_Free (m);
  Matrix_Free (U);
  Matrix_Free (H);

  return;
}

/* Computes the validity lattice of a system of equalities. By validity 
 * lattice, we mean all the integer values (of a sub-set of variables) 
 * for which the system of equalities admits integer solutions */

Matrix *validity_lattice (Matrix * MEqus)
{
  unsigned int i, j, m, k;
  m = MEqus->NbRows;
  k = MEqus->NbColumns - m - 2;
  Matrix *M = Matrix_Alloc (2 * m + k + 1, 2 * m + k + 1);
  Matrix *U, *L = Matrix_Alloc (k + 1, k + 1);
  Vector *T;
  
  for (i = 0; i < m; i++)
    {
      for (j = 0; j < m; j++)
	value_oppose (M->p[i][j], MEqus->p[i][j + 1]);
      for (j = 0; j < k; j++)
	value_assign (M->p[i + m][j + m], MEqus->p[i][j + m + 1]);
      value_set_si (M->p[i][m + k + i], 1);
      value_set_si (M->p[m + i][m + k + i], 1);
      value_assign (M->p[i + m][2 * m + k], MEqus->p[i][m + k + 1]);
    }
  value_set_si (M->p[2 * m + k][2 * m + k], 1);
  /*  Matrix_Print(stdout, P_VALUE_FMT, M);*/
  if (SolveDiophantine (M, &U, &T) == -1)
    {
      /*  printf("\n tttt  \n"); */		
      Matrix_Free (M);
      Matrix_Free (L);
      return NULL;
    }
  assert (U->NbColumns == k);
  for (i = 0; i < k; i++)
    {
      value_assign (L->p[i][k], T->p[m + i]);
      for (j = 0; j < k; j++) 
	value_assign (L->p[i][j], U->p[i + m][j]);
    }
  value_set_si (L->p[k][k], 1);
  Matrix_Free (M);
  Matrix_Free (U);
  Vector_Free (T);
  return L;
}

/* Used by function Split_NonCoprime to extract lattices containing points 
 * of the integer projection wich are outside the dark shadow */

LatticeUnion *lattice_prj (Matrix ** MC, Value val, int gamma, unsigned int *offset)
{
  int i, min;
  LatticeUnion *LL, *LU = NULL;
  *offset = 0;
  min = (gamma < VALUE_TO_INT (val) - 1) ? gamma : VALUE_TO_INT (val) - 1;
  for (i = 0; i <= min; i++)
    {
      if (MC[i] != NULL)
	{
	  LL = LatticeUnion_Alloc ();
	  *offset = 1;
	  LL->M = Matrix_Copy (MC[i]);
	  LL->next = LU;
	  LU = LL;
	}
    }
  return LU;
}


Matrix **Matrix_Compress (Vector * V, Value val, int min)
{
  int i;
  Matrix *MC;
  Matrix **res;
  res = (Matrix **)malloc ((min + 1) * sizeof (Matrix *));
  MC = Matrix_Alloc (1, V->Size + 2);
  value_set_si (MC->p[0][0], 0);
  value_assign (MC->p[0][1], val);
  Vector_Copy (V->p, MC->p[0] + 2, V->Size);
  for (i = 0; i <= min; i++)
    { 
      /*      Matrix_Print(stdout, P_VALUE_FMT, MC);*/
      res[i] = validity_lattice (MC);
      value_decrement (MC->p[0][(V->Size) + 1], MC->p[0][(V->Size) + 1]);
    }
  Matrix_Free (MC);
  return res;
}

/* When the coefficents of the existential variable are not coprime, this 
 * function computes the hyperplanes and the corresponding lattices containing 
 * the points of the integer projection. Only integer points which are outside 
 * the dark shadow are taken into account */

UZPolyhedron *Split_NonCoprime (Polyhedron * P, Vector * V1, Vector * V2, 
				Value * alpha, Value * beta, Value * g)
{
  Value gcd, one, tmp, som, abs_bet, abs_alp;
  int gamma, tp, min, cte;
  unsigned dim = P->Dimension, i, offset;
  Matrix *MC, *M = Matrix_Alloc (1, dim + 2);
  LatticeUnion *L;
  Polyhedron *Q;
  Matrix **MU;
  UZPolyhedron *UZP = NULL, *UTZ;

  value_init (gcd);
  value_init (one);
  value_init (tmp);
  value_init (som);
  value_init (abs_bet);
  value_init (abs_alp);
  value_set_si (one, 1);
  value_absolute (abs_bet, *beta);
  value_absolute (abs_alp, *alpha);
  value_multiply (tmp, abs_alp, abs_bet);
  value_substract (tmp, tmp, abs_alp);
  value_substract (tmp, tmp, abs_bet);
  value_set_si (M->p[0][0], 0);
  for (i = 1; i <= dim; i++)
    value_assign (M->p[0][i], P->Constraint[0][i]);
  value_addto (M->p[0][dim + 1], P->Constraint[0][dim + 1], tmp);
  value_increment (M->p[0][dim + 1], M->p[0][dim + 1]);
  Vector_AntiScale (M->p[0] + 1, M->p[0] + 1, *g, dim + 1);
  value_division (tmp, tmp, *g);
  Vector_Gcd (M->p[0] + 1, dim, &gcd);
  cte = VALUE_TO_INT (tmp);

  if (value_lt (abs_alp, abs_bet))
    {
      min =	(cte < VALUE_TO_INT (abs_alp) - 1) ? cte : VALUE_TO_INT (abs_alp) - 1;
      MU = Matrix_Compress (V1, abs_alp, min);
    }
  else
    {
      min = (cte < VALUE_TO_INT (abs_bet) - 1) ? cte : VALUE_TO_INT (abs_bet) - 1;
      /*	Vector_Print(stdout, P_VALUE_FMT, V2); */
      MU = Matrix_Compress (V2, abs_bet, min);
    }

  for (gamma = 0; gamma <= cte; gamma++)
    {
      value_set_si (tmp, -gamma);
      value_addto (tmp, tmp, M->p[0][dim + 1]);
      value_modulus (som, tmp, gcd);

      if (value_zero_p (som))
	break;
    }
  for (; gamma <= cte; gamma+=VALUE_TO_INT(gcd) )
    {
      value_set_si (tmp, gamma);
      value_substract (M->p[0][dim + 1], M->p[0][dim + 1], tmp);
      MC = Matrix_Copy (M);
      Q = Constraints2Polyhedron (MC, MAXRAYS);
      Matrix_Free (MC);
      if (value_lt (abs_alp, abs_bet))
	{			/*  lt gt   */
	  tp = gamma * (VALUE_TO_INT (*g)) / VALUE_TO_INT (abs_bet);
	  L = lattice_prj (MU, abs_alp, tp, &offset);
	}
      else
	{
	  tp = gamma * (VALUE_TO_INT (*g)) / VALUE_TO_INT (abs_alp);
	  L = lattice_prj (MU, abs_bet, tp, &offset);
	}
      if (offset)
	{
	  if (L)
	    {
	      UTZ = UZPolyhedron_Set (L, Q);
	      UTZ->next = UZP;
	      UZP = UTZ;
	    }
	}
      value_addto (M->p[0][dim + 1], M->p[0][dim + 1], tmp);
	
    }
  for (tp = 0; tp < min; tp++)
    if (MU[tp])
      Matrix_Free (MU[tp]);
  free (MU);

  Matrix_Free (M);
  value_clear (tmp);
  value_clear (gcd);
  value_clear (one);
  value_clear (som);
  value_clear (abs_bet);
  value_clear (abs_alp);
  return UZP;
}

/* Computes the lattices containing the integer projection of a pair of bounds in 
 * a particular case (when alpha.lower(x,p) - beta.upper(x,p) = "a constant") */

LatticeUnion *LatticeShadow (Vector * V1, Vector * V2, Value * alpha, Value * beta)
{
  Value abs_alp, abs_bet, tmp, t;
  LatticeUnion *L;
  unsigned int offset = 0, dim = V1->Size;
  int min, tp;
  Matrix **MU;

  value_init (t);
  value_init (tmp);
  value_init (abs_bet);
  value_init (abs_alp);
  value_absolute (abs_bet, *beta);
  value_absolute (abs_alp, *alpha);

  if (value_lt (abs_alp, abs_bet))
    {
      value_multiply (tmp, abs_alp, V2->p[dim - 1]);
      value_multiply (t, abs_bet, V1->p[dim - 1]);
      value_addto (tmp, tmp, t);
      value_division (tmp, tmp, abs_bet);
      tp = VALUE_TO_INT (tmp);
      min = (tp < VALUE_TO_INT (abs_alp) - 1) ? tp : VALUE_TO_INT (abs_alp) - 1;
      MU = Matrix_Compress (V1, abs_alp, min);
      L = lattice_prj (MU, abs_alp, tp, &offset);
    }
  else
    {
      value_multiply (tmp, abs_alp, V2->p[dim - 1]);
      value_multiply (t, abs_bet, V1->p[dim - 1]);
      value_addto (tmp, t, tmp);
      value_division (tmp, tmp, abs_alp);
      tp = VALUE_TO_INT (tmp);
      min = (tp < VALUE_TO_INT (abs_bet) - 1) ? tp : VALUE_TO_INT (abs_bet) - 1;
      MU = Matrix_Compress (V2, abs_bet, min);
      L = lattice_prj (MU, abs_bet, tp, &offset);
    }
  for (tp = 0; tp < min; tp++)
    if (MU[tp])
      Matrix_Free (MU[tp]);
  free (MU);
  return L;
}

LatticeUnion *LatUnionCopy (LatticeUnion * LU)
{
  LatticeUnion *res = NULL, *TMP;
  if (!LU)
    {
      fprintf (stderr, "LatUnionCopy : NULL LatticeUnion !");
      return NULL;
    }
  else
    {
      TMP = (LatticeUnion *)malloc (sizeof (LatticeUnion));
      TMP->M = Matrix_Copy (LU->M);
      TMP->next = NULL;
      res = TMP;
      if (LU->next)
	res->next = LatUnionCopy (LU->next);
    }
  return res;
}

/* Projects a polytope by eliminating one existential variable (the first 
 * variable in its constraint description). The result is given as a polytope
 * (dark shadow) and a union of Z-polytopes (Splits). */

SplitP *Project (Polyhedron * P, unsigned NbEx)
{
  Polyhedron *Exact, *TP1, *TP3, *T1, *T2, *Q, *CP;
  Polyhedron *Coprime_P = NULL, *Cop_P = NULL;
  UZPolyhedron *NonCoprime_P = NULL, *NonCop_P = NULL, *ZTP1, *ZP, *Z2;
  Matrix *M, *U;
  LatticeUnion *LL = NULL, *LT, *L;
  int splt, lcte, rcte, first = 1, bl;
  unsigned dim, NbC, i, j, k;
  Value alpha, beta, alpha_p, beta_p, tmp1, tmp2;
  Vector *V1, *V2, *T;
  SplitP *R = (SplitP *)malloc (sizeof (SplitP));
  dim = P->Dimension - 1;

  M = Matrix_Alloc (P->NbRays, dim + 2);
  for (k = 0; k < P->NbRays; k++)
    {
      value_assign (M->p[k][0], P->Ray[k][0]);
      Vector_Copy (P->Ray[k] + 2, M->p[k] + 1, dim + 1);
    }
  Exact = Rays2Polyhedron (M, MAXRAYS);
  Matrix_Free (M);

  if (emptyQ (Exact))
    {
      R->P = Empty_Polyhedron (dim);
      R->UZP = NULL;
      return R;
    }
  value_init (alpha);
  value_init (alpha_p);
  value_init (beta);
  value_init (beta_p);
  value_init (tmp1);
  value_init (tmp2);
  NbC = P->NbConstraints;
  V1 = Vector_Alloc (dim + 1);
  V2 = Vector_Alloc (dim + 1);

  for (i = 0; i < NbC; i++)
    {
      Polyhedron *TP2 = NULL;
      value_assign (alpha, P->Constraint[i][1]);
      for (j = i + 1; j < NbC; j++)
	{
	  value_assign (beta, P->Constraint[j][1]);
	  Vector_Copy (P->Constraint[i] + 2, V1->p, dim + 1);
	  Vector_Copy (P->Constraint[j] + 2, V2->p, dim + 1);

	  splt = 0;
	  lcte = rcte = 1;
	  for (k = 0; k < dim; k++)
	    {
	      if (value_notzero_p (V1->p[k]))
		{
		  lcte = 0;
		  break;
		}
	    }
	  for (k = 0; k < dim; k++)
	    {
	      if (value_notzero_p (V2->p[k]))
		{
		  rcte = 0;
		  break;
		}
	    }

	  if ((value_neg_p (alpha) && value_pos_p (beta))
	      || (value_pos_p (alpha) && value_neg_p (beta)))
	    {
	      bl = 0;
	      if (value_one_p (alpha) || value_one_p (beta) || value_mone_p (alpha) 
		  || value_mone_p (beta))
		{
		  Matrix *mt;
		  TP2 = Exact_Shadow (V1, V2, &alpha, &beta, &mt, dim);
		}
	      else
		{
		  if (lcte || rcte)
		    {
		      if (lcte)
			{
			  TP2 = Const_Shadow (V1, V2, &alpha, &beta, dim);
			}
		      else
			{
			  TP2 = Const_Shadow (V2, V1, &beta, &alpha, dim);
			}
		    }
		  else
		    {
		      TP2 = Dark_Shadow (V1, V2, &alpha, &beta, dim);
		      if (emptyQ (TP2))
			{
			  bl = 1;
			  for (k = 0; k < dim; k++)
			    {
			      value_multiply (tmp1, alpha, V2->p[k]);
			      value_multiply (tmp2, beta, V1->p[k]);
			      value_substract (tmp1, tmp1, tmp2);
			      if (value_notzero_p (tmp1))
				{
				  bl = 0;
				  break;
				}
			    }
			}
		      if (bl)
			{
			  Domain_Free (TP2);
			  LatticeUnion *LT1;
			  LT = LatticeShadow (V1, V2, &alpha, &beta);
			  if (LL == NULL)
			    {
			      LL = LT;
			    }
			  else
			    {
			      LT1 = LL;
			      LL = LatUnionIntersection (LL, LT);
			      LatticeUnion_Free (LT);
			      LatticeUnion_Free (LT1);
			    }
			}
		      else
			{
			  splt = 1;
			}
		    }
		}
	      if (splt && !PolyhedronIncludes (TP2, Exact))
		{
		  value_absolute (alpha_p, alpha);
		  value_absolute (beta_p, beta);
		  Gcd (alpha_p, beta_p, &tmp1);
		  if (value_one_p (tmp1))
		    {
		      M = Matrix_Alloc (3, 3);
		      value_assign (M->p[0][0], alpha_p);
		      value_assign (M->p[0][1], beta_p);
		      value_set_si (M->p[0][2], -1);
		      value_set_si (M->p[2][2], 1);
		      //T = (Vector **)malloc (sizeof (Vector *));
		      //U = malloc (sizeof (Matrix *));
		      SolveDiophantine (M, &U, &T);
		      value_assign (tmp2, T->p[0]);
		      Matrix_Free (U);
		      Matrix_Free (M);
		      Vector_Free (T);
		    }
		  if (value_one_p (tmp1))
		    {
		      Cop_P = Split_Coprime (TP2, &alpha, &beta, &tmp2, NbEx);
		      TP1 = Cop_P;
		      Cop_P = DomainIntersection (Cop_P, Exact, MAXRAYS);
		      Domain_Free (TP1);
		      TP2->next = Cop_P;
		      Cop_P = TP2;
		      if (first)
			{
			  Coprime_P = Cop_P;
			}
		      else
			{
			  TP1 = NULL;
			  for (T1 = Coprime_P; T1; T1 = T1->next)
			    {
			      for (T2 = Cop_P; T2; T2 = T2->next)
				{
				  Q = AddConstraints (T2->Constraint[0],
						      T2->NbConstraints, T1, MAXRAYS);
				  PolyhedronSimplify (&Q, MAXRAYS);
				  CP = Polyhedron_Trancate2 (Q, MAXRAYS);
				  if (CP) {
				    Domain_Free(Q);
				    Q=CP;					 
			    	  }

				  if (Q != NULL && !emptyQ (Q))
				    {
				      Q->next = TP1;
				      TP1 = Q;
				    }
				}
			    }
			  Domain_Free (Coprime_P);
			  Coprime_P = TP1;
			}
		      if (NonCoprime_P)
			{
			  UZPolyhedron *Z1 = NULL;
			  for (TP2 = Cop_P; TP2; TP2 = TP2->next)
			    {
			      Q = TP2->next;
			      TP2->next = NULL;
			      for (ZTP1 = NonCoprime_P; ZTP1;
				   ZTP1 = ZTP1->next)
				{
				  TP3 = DomainIntersection (ZTP1->P, TP2,	MAXRAYS);
				  if (!emptyQ (TP3))
				    {
				      LT = LatUnionCopy (ZTP1->LU);
				      ZP = UZPolyhedron_Set (LT, TP3);
				      SimplifyUZPoly (&ZP, MAXRAYS);
				      if (ZP)
					{
					  ZP->next = Z1;
					  Z1 = ZP;
					}
				    }
				}
			      TP2->next = Q;
			    }
			  UZDomain_Free (NonCoprime_P);
			  NonCoprime_P = Z1;
			  Domain_Free (Cop_P);
			}
		    }
		  else
		    {
		      UZPolyhedron *Z1 = NULL, *Z2;
		      NonCop_P =
			Split_NonCoprime (TP2, V1, V2, &alpha, &beta, &tmp1);
		      for (ZTP1 = NonCop_P; ZTP1; ZTP1 = ZTP1->next)
			{
			  TP1 = ZTP1->P;
			  ZTP1->P =
			    DomainIntersection (ZTP1->P, Exact, MAXRAYS);
			  Domain_Free (TP1);
			}
		      for (TP1 = Coprime_P; TP1; TP1 = TP1->next)
			{
			  Q = TP1->next;
			  TP1->next = NULL;
			  for (ZTP1 = NonCop_P; ZTP1; ZTP1 = ZTP1->next)
			    {
			      TP3 =
				DomainIntersection (TP1, ZTP1->P, MAXRAYS);
			      if (!emptyQ (TP3))
				{
				  LT = LatUnionCopy (ZTP1->LU);
				  ZP = UZPolyhedron_Set (LT, TP3);
				  SimplifyUZPoly (&ZP, MAXRAYS);
				  if (ZP)
				    {
				      ZP->next = Z1;
				      Z1 = ZP;
				    }
				}
			    }
			  TP1->next = Q;
			}
		      for (Z2 = NonCoprime_P; Z2; Z2 = Z2->next)
			{
			  for (ZTP1 = NonCop_P; ZTP1; ZTP1 = ZTP1->next)
			    {
			      TP3 = DomainIntersection (Z2->P, ZTP1->P, MAXRAYS);
			      if (!emptyQ (TP3))
				{
				  L = LatUnionIntersection (Z2->LU, ZTP1->LU);
				  if (L)
				    {
				      LT = LatUnionCopy (L);
				      ZP = UZPolyhedron_Set (LT, TP3);
				      SimplifyUZPoly (&ZP, MAXRAYS);
				      if (ZP)
					{
					  ZP->next = Z1;
					  Z1 = ZP;
					}
				    }
				}
			    }
			}
		      if (first)
			{
			  Coprime_P = TP2;
			  NonCoprime_P = NonCop_P;
			}
		      else
			{
			  TP1 = Coprime_P;
			  TP3 = NULL;
			  for (T1 = Coprime_P; T1; T1 = T1->next)
			    {
			      Q = AddConstraints (T1->Constraint[0],
						  T1->NbConstraints, TP2,
						  MAXRAYS);
			      PolyhedronSimplify (&Q, MAXRAYS);
			      CP = Polyhedron_Trancate2 (Q, MAXRAYS);
			      if (CP) {
				Domain_Free(Q);
				Q=CP;					 
			      }
			      if (!emptyQ (Q))
				{
				  Q->next = TP3;
				  TP3 = Q;
				}
			    }
			  Coprime_P = TP3;
			  Domain_Free (TP1);
			  Z2 = NULL;
			  for (ZTP1 = NonCoprime_P; ZTP1; ZTP1 = ZTP1->next)
			    {
			      Q =
				AddConstraints (ZTP1->P->Constraint[0],
						ZTP1->P->NbConstraints, TP2, MAXRAYS);
			      PolyhedronSimplify (&Q, MAXRAYS);
			      CP = Polyhedron_Trancate2 (Q, MAXRAYS);
			      if (CP) {
				Domain_Free(Q);
				Q=CP;					 
			      }
			      if (!emptyQ (Q))
				{
				  LT = LatUnionCopy (ZTP1->LU);
				  ZP = UZPolyhedron_Set (LT, Q);
				  SimplifyUZPoly (&ZP, MAXRAYS);
				  if (ZP)
				    {
				      ZP->next = Z2;
				      Z2 = ZP;
				    }

				}
			    }
			  UZDomain_Free (NonCoprime_P);
			  NonCoprime_P = Z2;

			  if (NonCoprime_P == NULL)
			    {
			      NonCoprime_P = Z1;
			    }
			  else
			    {
			      for (ZTP1 = NonCoprime_P; ZTP1->next;
				   ZTP1 = ZTP1->next);
			      ZTP1->next = Z1;
			    }
			  UZDomain_Free (NonCop_P);
			  Domain_Free (TP2);
			}
		    }
		  first = 0;
		}
	      else
		{
		  if (!bl)
		    {
		      if (first)
			{
			  Coprime_P = TP2;
			}
		      else
			{
			  TP1 = Coprime_P;
			  Coprime_P = DomainIntersection (Coprime_P, TP2, MAXRAYS);
			  Domain_Free (TP1);
			}
		      if (NonCoprime_P)
			{
			  for (ZTP1 = NonCoprime_P; ZTP1; ZTP1 = ZTP1->next)
			    {
			      TP1 = ZTP1->P;
			      ZTP1->P = DomainIntersection (TP1, TP2, MAXRAYS);
			      Domain_Free (TP1);
			    }
			}
		      if (!first)
			Domain_Free (TP2);
		      first = 0;
		    }
		}
	    }
	}
    }

  if (Coprime_P && !emptyQ (Coprime_P))
    {
      TP1 = Coprime_P;
      Coprime_P = DomainIntersection (Coprime_P, Exact, MAXRAYS);
      Domain_Free (TP1);
    }
  Z2 = NULL;
  ZP = NonCoprime_P;
  while (ZP)
    {
      ZTP1 = ZP->next;
      ZP->next = NULL;
      TP1 = ZP->P;
      ZP->P = DomainIntersection (ZP->P, Exact, MAXRAYS);
      PolyhedronSimplify (&(ZP->P), MAXRAYS);

      CP = Polyhedron_Trancate2 (ZP->P, MAXRAYS);
      if (CP) {
	Domain_Free(ZP->P);
	ZP->P=CP;					 
      }
      if (!emptyQ (ZP->P))
	{
	  ZP->next = Z2;
	  Z2 = ZP;
	}
      else
	{
	  UZDomain_Free (ZP);
	}
      Domain_Free (TP1);
      ZP = ZTP1;
    }
  NonCoprime_P = Z2;
  Domain_Free (Exact);

  if (LL == NULL)
    {
      R->P = Coprime_P;
      R->UZP = NonCoprime_P;
    }
  else
    {
      ZTP1 = NULL;
      UZPolyhedron *ZTP;
      for (ZP = NonCoprime_P; ZP; ZP = ZP->next)
	{
	  LT = LatUnionIntersection (ZP->LU, LL);
	  if (LT)
	    {
	      ZTP = UZPolyhedron_Set (LT, Polyhedron_Copy (ZP->P));
	      SimplifyUZPoly (&ZTP, MAXRAYS);
	      if (ZTP)
		{
		  ZTP->next = ZTP1;
		  ZTP1 = ZTP;
		}
	    }
	}
      TP1 = Coprime_P;
      while (TP1)
	{
	  Q = TP1->next;
	  TP1->next = NULL;
	  LT = LatUnionCopy (LL);
	  ZTP = UZPolyhedron_Set (LT, TP1);
	  SimplifyUZPoly (&ZTP, MAXRAYS);
	  if (ZTP)
	    {
	      ZTP->next = ZTP1;
	      ZTP1 = ZTP;
	    }
	  TP1 = Q;
	}

      LatticeUnion_Free (LL);
      if (NonCoprime_P != NULL)
	UZDomain_Free (NonCoprime_P);
      R->P = NULL;
      R->UZP = ZTP1;
    }

  Vector_Free (V1);
  Vector_Free (V2);
  value_clear (alpha);
  value_clear (alpha_p);
  value_clear (beta);
  value_clear (beta_p);
  value_clear (tmp1);
  value_clear (tmp2);
  return R;
}

int find_min(Polyhedron *P, int n_var, int *offset){
  unsigned i;
  double minf=0, tmp; 
  for(i=0; i< P->NbRays; i++) {
    if (value_one_p(P->Ray[i][0]) 
	&& value_notzero_p(P->Ray[i][P->Dimension+1])) {
      minf = (double) VALUE_TO_INT(P->Ray[i][n_var+1]) / (double) VALUE_TO_INT(P->Ray[i][P->Dimension+1]);
      break;
    }
  }
  for(; i< P->NbRays; i++) {
    if (value_one_p(P->Ray[i][0]) 
	&& value_notzero_p(P->Ray[i][P->Dimension+1])) {
      tmp = (double) VALUE_TO_INT(P->Ray[i][n_var+1]) / (double) VALUE_TO_INT(P->Ray[i][P->Dimension+1]);
      if (tmp < minf) 
	minf = tmp;
    }
  }
  tmp = ceil(minf);
  if (tmp > minf)
    *offset = 1;

  return (int) tmp;
}

int find_max(Polyhedron *P, int n_var, int *offset){
  unsigned i;
  double maxf=0, tmp;

  for(i=0; i< P->NbRays; i++) {
    if (value_one_p(P->Ray[i][0]) 
	&& value_notzero_p(P->Ray[i][P->Dimension+1])) {
      maxf = (double) VALUE_TO_INT(P->Ray[i][n_var+1]) / (double) VALUE_TO_INT(P->Ray[i][P->Dimension+1]);
      break;
    }
  }
  for(; i< P->NbRays; i++) {
    if (value_one_p(P->Ray[i][0]) 
	&& value_notzero_p(P->Ray[i][P->Dimension+1])) {
      tmp = (double) VALUE_TO_INT(P->Ray[i][n_var+1]) / (double) VALUE_TO_INT(P->Ray[i][P->Dimension+1]);
      if (tmp > maxf) 
	maxf = tmp;
    }
  }
  tmp = floor(maxf);
  if (tmp < maxf) 
    *offset = 1;

  return (int) tmp;
}


Polyhedron * Polyhedron_Trancate2(Polyhedron * P, unsigned MaxRays) {
  unsigned i, j, dim=P->Dimension;
  int bl=0, offset, allP, allN, mi, ma, * min, *max;
  Polyhedron *Q, *R=NULL;
  Matrix *M;

  min=(int*)malloc(dim*sizeof(int *));
  max=(int*)malloc(dim*sizeof(int *));
        
  for (i=0; i< dim; i++) {
    min[i]= max[i]=1;
  }

  for (i=0; i< P->NbRays; i++) {
    if (value_zero_p(P->Ray[i][0])) {
      for(j=1; j<=dim; j++) {
	if(value_notzero_p(P->Ray[i][j])){
	  min[j-1]= max[j-1]=0;
	}	 
      }
    }
  }
  for (i=0; i < dim; i++) {
    if (min[i]) {
      allP=1; allN=1;
      for(j=0; j< P->NbRays; j++) {
	if(value_one_p(P->Ray[j][0])
	   && value_zero_p(P->Ray[j][dim+1])){
	  if value_neg_p(P->Ray[j][i+1])
			  allP=0;
	  else if (value_pos_p(P->Ray[j][i+1]))
	    allN=0;
	}

      }
      if (!allP)
	min[i]=0;
      if (!allN)
	max[i]=0;
    }
  } 
      
  for (i=0; i< dim; i++) { 
    if (min[i]||(max[i])) {
      bl=1;
      break;
    }		 
  }

  if (bl) {
    Q=Polyhedron_Copy(P);
    M=Matrix_Alloc(1, Q->Dimension+2); 
    for (i=0; i< dim; i++) {
      for(j=1; j<dim+2; j++) 
	value_set_si(M->p[0][j], 0);
      mi=ma=0;
      if (min[i]) { 
	offset = 0;
	mi=find_min(P, i, &offset);
	if(offset) {
	  value_set_si(M->p[0][0], 1);
	  value_set_si(M->p[0][i+1], 1);
	  value_set_si(M->p[0][dim+1], -mi);
	  R=DomainAddConstraints(Q,M, MAXRAYS);
	  Domain_Free(Q);
	  Q=R;
	}
      }
      if (max[i]) {
	offset = 0;
	ma=find_max(P, i, &offset);
	if(offset) {
	  value_set_si(M->p[0][0], 1);
	  value_set_si(M->p[0][i+1], -1);
	  value_set_si(M->p[0][dim+1], ma);
	  R=DomainAddConstraints(Q,M, MAXRAYS);
	  Domain_Free(Q);
	  Q=R;
	}
      }
      if(mi > ma) {
	break;
      }
    }
    free(min);
    free(max);
    Matrix_Free(M);
    return Q;
  }
  else 
    {
      free(min);
      free(max);
      return NULL;
    }
}

static Polyhedron * Variable_Exchange (Polyhedron * P, unsigned n_exi) {
  Polyhedron *Q=NULL;
  Matrix *M;
  unsigned i, j=1, k, l, bl=0;
  Value val;

  for (i=0; i< P->NbConstraints; i++) {
    if (value_zero_p(P->Constraint[i][0])) {
      if (value_zero_p(P->Constraint[i][j])) {
	bl=1;
	break; 
      }
      j++;
    }
  }
  if (bl) {
    value_init(val);
    M=Matrix_Alloc(P->NbConstraints, P->Dimension+2);
    for (i=0; i<P->NbConstraints; i++) 
      for (j=0; j< P->Dimension + 2; j++) 
	value_assign(M->p[i][j], P->Constraint[i][j]);
    j=1;          
    for (i=0; i< M->NbRows; i++) {
      if (value_zero_p(M->p[i][0])) {
	if (value_zero_p (M->p[i][j])) {
	  for (k=j+1; k<=n_exi; k++)
	    if (value_notzero_p(M->p[i][k])) {
	      for (l=0; l< M->NbRows; l++) {
		value_assign (val, M->p[l][j]);
		value_assign (M->p[l][j], M->p[l][k]);
		value_assign (M->p[l][k], val);
	      }
	      break; 
	    }  
	}
	j++;
      } 
    }
    value_clear (val);
    Q=Constraints2Polyhedron(M, MAXRAYS);
  }
  return Q;
}

/* Eliminates (in Z) the equalities of a polytope. The result is said a Z-polytope.
 * That is the intersection of a full-dimensional polytope and a validity lattice */

ZPolyhedron *Poly2Zpoly (Polyhedron * P, int *n_exi, unsigned dprj)
{
  ZPolyhedron *ZP;
  Polyhedron *Q;
  Matrix *M, *T, *L;
  int i, j, nbc = P->NbConstraints, dim = P->Dimension, bl, nb_e, *vec;

  nb_e = 0;
  vec = (int *)calloc (nbc, sizeof (int));
  Q=Variable_Exchange(P, *n_exi);
  if (Q) 
    P=Q;
  for (i = 0; i < nbc; i++)
    {
      if (value_zero_p (P->Constraint[i][0]))
	{
	  bl = 0;
	  for (j = 1; j <= *n_exi; j++)
	    {
	      if (value_notzero_p (P->Constraint[i][j]))
		{
		  vec[i] = 1;
		  bl = 1;
		  break;
		}
	    }
	  if (bl)
	    nb_e++;
	}
    }

  M = Matrix_Alloc (nb_e, dim + 2);
  j = 0;
  for (i = 0; j < nb_e; i++)
    {
      if (vec[i])
	Vector_Copy (P->Constraint[i], M->p[j++], P->Dimension + 2);
    }

  T = Matrix_Alloc (nbc - nb_e, dim + 2 - nb_e);
  j = 0;
  for (i = 0; i < nbc; i++)
    {
      if (!vec[i])
	{
	  value_assign (T->p[j][0], P->Constraint[i][0]);
	  Vector_Copy (P->Constraint[i] + nb_e + 1, T->p[j++] + 1, dim - nb_e + 1);
	}
    }
  Q = Constraints2Polyhedron (T, MAXRAYS);
  *n_exi -= nb_e;
  L = validity_lattice (M);
  Matrix_Free(M);

  if (!L)
    {
      Matrix_Free (T);
      Domain_Free (Q);
      free (vec);
      return NULL;
    }
  NormalizeLattice (&L, dprj);

  ZP = ZPolyhedron_Alloc (L, Q);
  Matrix_Free (L);
  Matrix_Free (T);
  Domain_Free (Q);
  free (vec);
  return ZP;
}

int Nul_evalue (evalue * ev)
{
  int nul = 0;
  if (value_notzero_p (ev->d))
    {
      if (value_zero_p (ev->x.n))
	nul = 1;
    }
  return nul;
}

/* Changes or keeps (by means of a heuristic) the order of the existential variables 
 * before eliminating the equalities. This may (in some cases) reduce the existential 
 * variable elimination complexity */

void PreChooseOrder (Matrix ** MP, unsigned n_exi)
{
  int *size, *ind_m;
  unsigned i, j, k, ii, t, n_eq = 0;
  Matrix *Mat, *MatInv;
  Value tmp;
  value_init (tmp);

  for (i = 0; i < (*MP)->NbRows; i++)
    if (value_zero_p (((*MP)->p[i][0])))
      n_eq++;

  if (n_eq == 0 || n_exi < 2 || n_eq >= n_exi)
    return;

  ind_m = (int *)malloc (n_eq * sizeof (int));
  size = (int *)malloc (n_exi * sizeof (int));

  for (i = 0; i < n_exi; i++)
    {
      size[i] = 0;
      if (i < n_eq)
	ind_m[i] = 0;
    }

  Mat = Matrix_Alloc (n_eq, n_eq);
  MatInv = Matrix_Alloc (n_eq, n_eq + 1);

  for (k = 1; k <= n_exi; k++)
    {
      for (i = 0; i < (*MP)->NbRows; i++)
	{
	  size[k - 1] += abs (VALUE_TO_INT ((*MP)->p[i][k]));
	}
    }

  for (i = 0; i < n_eq; i++)
    {
      unsigned m, n;
      int inv, inv2;
      k = i;
      for (n = 0; n < n_eq; n++)
	for (m = 0; m < n_eq; m++)
	  value_assign (Mat->p[n][m], (*MP)->p[n][m + 1]);
      inv = MatInverse (Mat, MatInv);
      for (j = i + 1; j < n_exi; j++)
	{
	  for (n = 0; n < n_eq; n++)
	    value_assign (Mat->p[n][i], (*MP)->p[n][j + 1]);
	  inv2 = MatInverse (Mat, MatInv);
	  if ((size[j] < size[k] && inv2) || !inv)
	    {
	      k = j;
	    }
	}
      if (k != i)
	{
	  t = size[i];
	  size[i] = size[k];
	  size[k] = t;
	  for (ii = 0; ii < (*MP)->NbRows; ii++)
	    {
	      value_assign (tmp, (*MP)->p[ii][k + 1]);
	      value_assign ((*MP)->p[ii][k + 1], (*MP)->p[ii][i + 1]);
	      value_assign ((*MP)->p[ii][i + 1], tmp);
	    }
	}
    }
  Matrix_Free (Mat);
  Matrix_Free (MatInv);
  free (size);
  value_clear (tmp);
  return;
}

/* Calculates (by means of a heuristic) an integer value used to decide whether
 * to project by simply adding facets. This is possible only when there is one 
 * existential variable to be eliminated and when "USING_ONE_DIM" is defined */

int NbTests (Polyhedron * P)
{
  unsigned i, j;
  int res = 0;
  Value gcd, tmp, tmp1, tmp2;
  value_init (gcd);
  value_init (tmp);
  value_init (tmp1);
  value_init (tmp2);
  for (i = 0; i < P->NbConstraints; i++)
    {
      value_absolute (tmp2, P->Constraint[i][1]);
      for (j = i + 1; j < P->NbConstraints; j++)
	{
	  value_absolute (tmp, P->Constraint[j][1]);
	  if (value_notone_p (tmp) && value_notone_p (tmp2))
	    {
	      value_multiply (tmp1, P->Constraint[i][1], P->Constraint[j][1]);
	      if (value_neg_p (tmp1))
		{
		  value_multiply (tmp1, tmp, tmp2);
		  value_subtract (tmp1, tmp1, tmp);
		  value_subtract (tmp1, tmp1, tmp2);
		  Gcd (tmp, tmp2, &gcd);
		  value_division (tmp1, tmp1, gcd);
		  res += abs (VALUE_TO_INT (tmp1));
		}
	    }
	}
    }
  value_clear (gcd);
  value_clear (tmp);
  value_clear (tmp1);
  value_clear (tmp2);
  return res;
}

/* Changes or keeps (by means of a heuristic) the order of the existential variables 
 * after eliminating the equalities. This may (in some cases) reduce the existential 
 * variable elimination complexity */

void ChooseOrder (Polyhedron ** P, unsigned n_exi)
{
  Matrix *M = Matrix_Alloc ((*P)->NbConstraints, (*P)->Dimension + 2);
  int *n_pairs, *t_splits;
  unsigned i, j, k, t, change, ii;
  Value gcd, tmp, tmp1, tmp2;

  value_init (gcd);
  value_init (tmp);
  value_init (tmp1);
  value_init (tmp2);
  n_pairs = (int *)malloc (n_exi * sizeof (int));
  t_splits = (int *)malloc (n_exi * sizeof (int));

  for (i = 0; i < n_exi; i++)
    {
      n_pairs[i] = 0;
      t_splits[i] = 0;
    }

  for (i = 0; i < (*P)->NbConstraints; i++)
    Vector_Copy ((*P)->Constraint[i], M->p[i], (*P)->Dimension + 2);

  for (k = 1; k <= n_exi; k++)
    {
      for (i = 0; i < (*P)->NbConstraints; i++)
	{
	  value_absolute (tmp2, (*P)->Constraint[i][k]);
	  for (j = i + 1; j < (*P)->NbConstraints; j++)
	    {
	      value_absolute (tmp, (*P)->Constraint[j][k]);
	      if (value_notone_p (tmp) && value_notone_p (tmp2))
		{
		  value_multiply (tmp1, (*P)->Constraint[i][k], (*P)->Constraint[j][k]);
		  if (value_neg_p (tmp1))
		    {
		      n_pairs[k - 1]++;
		      value_multiply (tmp1, tmp, tmp2);
		      value_subtract (tmp1, tmp1, tmp);
		      value_subtract (tmp1, tmp1, tmp2);
		      Gcd (tmp, tmp2, &gcd);
		      value_division (tmp1, tmp1, gcd);
		      t_splits[k - 1] += abs (VALUE_TO_INT (tmp1));
		    }
		}
	    }
	}
    }

  change = 0;

  for (i = 0; i < n_exi - 1; i++)
    {
      k = i;
      for (j = i + 1; j < n_exi; j++)
	{
	  if (n_pairs[j] < n_pairs[i])
	    {
	      k = j;
	      change = 1;
	    }
	}
      if (k != i)
	{
	  t = n_pairs[i];
	  n_pairs[i] = n_pairs[k];
	  n_pairs[k] = t;
	  t = t_splits[i];
	  t_splits[i] = t_splits[k];
	  t_splits[k] = t;
	  for (ii = 0; ii < M->NbRows; ii++)
	    {
	      value_assign (tmp, M->p[ii][k + 1]);
	      value_assign (M->p[ii][k + 1], M->p[ii][i + 1]);
	      value_assign (M->p[ii][i + 1], tmp);
	    }
	}
      k = i;
      for (j = i + 1; j < n_exi; j++)
	{
	  if (t_splits[j] + 20 < t_splits[i] ||
	      (n_pairs[j] - 2 < n_pairs[i] && t_splits[j] < t_splits[i]))
	    {
	      k = j;
	      change = 1;
	    }
	}
      if (k != i)
	{
	  t = t_splits[i];
	  t_splits[i] = t_splits[k];
	  t_splits[k] = t;
	  t = n_pairs[i];
	  n_pairs[i] = n_pairs[k];
	  n_pairs[k] = t;
	  for (ii = 0; ii < M->NbRows; ii++)
	    {
	      value_assign (tmp, M->p[ii][k + 1]);
	      value_assign (M->p[ii][k + 1], M->p[ii][i + 1]);
	      value_assign (M->p[ii][i + 1], tmp);
	    }

	}
    }
  if (change)
    {
      Domain_Free (*P);
      *P = Constraints2Polyhedron (M, MAXRAYS);
    }
  Matrix_Free (M);
  value_clear (gcd);
  value_clear (tmp);
  value_clear (tmp1);
  value_clear (tmp2);
}

/*  Projects a polytope by simply adding facets. This is possible only when there is 
 *  one existential variable to be eliminated and when "USING_ONE_DIM" is defined */

Polyhedron *ProjectOneDim (Polyhedron * D, int NbExi)
{
  Polyhedron *polun, *pol;
  Matrix *mp, *full, *lattice;
  Vector *v1, *v2;
  Value h;
  unsigned i, j, k;

  POL_ENSURE_FACETS (D);
  POL_ENSURE_VERTICES (D);

  value_init (h);
  if (!D)
    {
      fprintf (stdout, " Error: in reading input domain \n");
      value_clear (h);
      return NULL;
    }

  if (emptyQ (D))
    {
      return NULL;
    }
  if (D->NbEq != 0)
    {
      unsigned dprj = D->Dimension - NbExi;
      mp = Matrix_Alloc (D->NbConstraints, D->Dimension + 2);
      for (i = 0; i < D->NbConstraints; i++)
	Vector_Copy (D->Constraint[i], mp->p[i], D->Dimension + 2);
      full = full_dimensionize (mp, dprj, &lattice);
      NbExi -= D->NbEq;
      D = Constraints2Polyhedron (full, MAXRAYS);
      Matrix_Free (full);
      Matrix_Free (mp);
      Matrix_Free (lattice);
    }

  assert (NbExi == 1);
  v1 = Vector_Alloc (D->Dimension);
  v2 = Vector_Alloc (D->Dimension);
  value_set_si (v1->p[0], 1);

  polun = (Polyhedron *) NULL;

  /* adding a new constraint for all constraints of D in which the scalar 
   * product of the  normal whith vector v1 is greter then zero */

  ///// choice scl prod > or < (nb const to add) 
  for (j = 0; j < D->NbConstraints; j++)
    {
      for (k = 0; k <= D->Dimension - 1; k++)
	{
	  value_assign (v2->p[k], D->Constraint[j][k + 1]);
	}
      Inner_Product (v1->p, v2->p, D->Dimension, &h);
      if (value_pos_p (h) && !value_zero_p (D->Constraint[j][0]))
	{
	  Vector *NCont;
	  Value val;
	  value_init (val);
	  /* Create a new contraint whitch is added to the polyhedron */

	  NCont = Vector_Alloc (D->Dimension + 2);
	  value_set_si (NCont->p[0], 1);	// the constraint is an inequality
	  for (k = 1; k <= D->Dimension; k++)
	    {
	      value_oppose (NCont->p[k], D->Constraint[j][k]);
	    }
	  value_decrement (val, h);
	  value_subtract (val, val, D->Constraint[j][D->Dimension + 1]);
	  value_assign (NCont->p[D->Dimension + 1], val);
	  //add the new constraint to polyhedron D
	  pol = AddConstraints (NCont->p, 1, D, MAXRAYS);
	  polun = AddPolyToDomain (Polyhedron_Copy (pol), polun);
	  Domain_Free (pol);
	  Vector_Free (NCont);
	  value_clear (val);
	}
    }

  Vector_Free (v1);
  Vector_Free (v2);
  if (polun == NULL)
    {				//  No constraint is added to input polyhedron
      return D;
    }
  else
    {				// some constraints are added to the input polyhedron 
      return polun;
    }
}

/* Checks whether the polytope is unbounded, i.e., contains an infinite number 
 * of integer points */

int Unbounded_Poly (Polyhedron * PP, unsigned n_par)
{
  Matrix *M = Matrix_Alloc (n_par + 1, PP->Dimension + 1);
  Polyhedron *P, *Q;
  unsigned i, j, n_var = PP->Dimension - n_par;
  int  bl = 0;
  for (i = 0; i <= n_par; i++)
    value_set_si (M->p[i][i + n_var], 1);
  P = Polyhedron_Image (PP, M, MAXRAYS);
  Matrix_Free (M);
  M = Matrix_Alloc (n_par, PP->Dimension + 2);
  for (i = 0; i < P->NbRays; i++)
    {
      if (value_notzero_p (P->Ray[i][n_par + 1]))
	{
	  for (j = 0; j < n_par; j++)
	    {
	      value_assign (M->p[j][j + PP->Dimension - n_par + 1], P->Ray[i][n_par + 1]);
	      value_oppose (M->p[j][PP->Dimension + 1], P->Ray[i][j + 1]);
	    }
	  break;
	}
    }

  if( n_par > 0 )
    Q = AddConstraints (M->p[0], M->NbRows, PP, MAXRAYS);
  else
    Q = PP;
  Matrix_Free (M);
  Domain_Free (P);
  if (Q->NbBid)
    bl = 1;
  else
    {
      for (i = 0; i < Q->NbRays; i++)
	if (value_zero_p (Q->Ray[i][Q->Dimension + 1]))
	  {
	    bl = 1;
	    break;
	  }
    }
  if( n_par > 0 )
    Domain_Free (Q);
  return bl;
}

/* This is the main projection function. Given a possibly non-full dimensional 
 * polytope with "NbExi" existential variables, this function eliminates all 
 * the existential variables and returns the result as a union of Z-polytopes */  

/* using_one_dim is an optimization:
 * - of the case where there is only one dimension to project out 
 * - to be used (set to 1) only when the resulting union of Z-polytopes is not
 *   used for something else than counting (the exact result is not computed,
 *   but an equivalent set) */
UZPolyhedron *Project_Polytope (Polyhedron * Pol, Polyhedron * C, int NbExi, int NbPoly, int using_one_dim)
{
  Polyhedron *Q, *PP = Polyhedron_Copy (Pol), *Coprime_P = NULL;
  Polyhedron *TP1, *TP2, *P, *PE = NULL;
  ZPolyhedron *Zpol;
  UZPolyhedron *NonCoprime_P = NULL, *ZPU = NULL, *ZTP1, *ZPU1 = NULL, *ZP;
  Matrix *M, *MM = NULL, *MT, *ME, *MIE;
  LatticeUnion *LU = NULL;
  Lattice *L;
  unsigned i, j, dprj;
  int single = 0;
		  SplitP *Rdark;

  //PolyhedronSimplify (&PP, MAXRAYS);
  Q = Polyhedron_Trancate2 (PP, MAXRAYS);
  if (Q) {
    Domain_Free(PP);
    PP=Q;
  }

  if (emptyQ (PP)) {
      printf ("\n Empty input polyhedron !!! \n\n");
      return NULL;
    }

  if (Unbounded_Poly (PP, C->Dimension)) {
      printf ("\n Unbounded input polyhedron !!! \n\n");
      return NULL;
    }
  
 
  dprj = PP->Dimension - NbExi;
  if (PP->NbEq != 0 && NbExi != 0) {
     	
    Zpol = Poly2Zpoly (PP, &NbExi, dprj);

    if (Zpol == NULL)	{
      printf ("\n Empty polyhedron !!!\n");
      return NULL;
    }

    int dim = Zpol->P->Dimension + 1 - NbExi;

    P = Polyhedron_Preimage (Zpol->P, Zpol->Lat, MAXRAYS);
    Q = Polyhedron_Trancate2 (P, MAXRAYS);
    if (Q) {
      Domain_Free(P);
      P=Q;
    }
    if (P->NbEq != 0) {
      unsigned k = 0, l;
      j = 0;
      ME = Matrix_Alloc (P->NbEq, dprj + 2);
      MIE = Matrix_Alloc (P->NbConstraints - P->NbEq, P->Dimension + 2);

      for (i = 0; i < P->NbConstraints; i++) {
	if (value_zero_p (P->Constraint[i][0])) {
	  for (l = 1; l <= P->Dimension - dprj; l++)
	    assert (value_zero_p (P->Constraint[i][l]));

	  value_set_si (ME->p[j][0], 0);
	  Vector_Copy (P->Constraint[i] + P->Dimension - dprj + 1,
		       ME->p[j++] + 1, dprj + 1);
	}
	else
	  Vector_Copy (P->Constraint[i], MIE->p[k++], P->Dimension + 2);
      }

      PE = Constraints2Polyhedron (ME, MAXRAYS);
      Domain_Free (P);
      P = Constraints2Polyhedron (MIE, MAXRAYS);
    }

    assert (P->NbEq == 0 || NbExi == 0);
    MM = Matrix_Alloc (dim, dim);

    for (i = 0; i < MM->NbRows; i++) 
      for (j = 0; j < MM->NbRows; j++) 
	value_assign (MM->p[i][j], Zpol->Lat->p[i + NbExi][j + NbExi]);
        
    ZDomain_Free (Zpol);
  }

  if (PP->NbEq != 0 && PP->Dimension != dprj) { 
    Polyhedron *Pt;
    Pt=Polyhedron_Copy (P);	
    PolyhedronSimplify (&P, MAXRAYS);

    if (P->NbEq != 0) {
      Domain_Free (P);
      P=Pt;
    }
    else {
      Domain_Free (Pt);
    }
    Domain_Free (PP);
  }
  else {
    P = Polyhedron_Copy (PP);
    PolyhedronSimplify (&P, MAXRAYS);
    if (P->NbEq != 0) {
      Domain_Free (P);
      P=PP;
    }
  }

  //#ifdef USING_ONE_DIM
  if (using_one_dim && NbExi == 1 && NbTests (P) > 20 && NbPoly == 1) {
    single = 1;
    ZPU = NULL;
    TP1 = ProjectOneDim (P, 1);
    M = Matrix_Alloc (P->Dimension + 1, P->Dimension + 1);
    for (i = 0; i <= P->Dimension; i++)
      value_set_si (M->p[i][i], 1);
    TP2 = TP1;
    while (TP2) {
      Q = TP2->next;
      TP2->next = NULL;
      LU = LatticeUnion_Alloc ();
      LU->M = Matrix_Copy (M);
      ZP = UZPolyhedron_Set (LU, TP2);
      ZP->next = ZPU;
      ZPU = ZP;
      TP2 = Q;
    }
  }

  //#endif

  if (NbExi == 0) {
    M = Matrix_Alloc (P->Dimension + 1, P->Dimension + 1);
    for (i = 0; i <= P->Dimension; i++)
      value_set_si (M->p[i][i], 1);
    LU = LatticeUnion_Alloc ();
    LU->M = M;
    ZPU = UZPolyhedron_Set (LU, P);
  }
  else {
    if (!single) {
      //PolyhedronSimplify (&P, MAXRAYS);
      Q = Polyhedron_Trancate2(P, MAXRAYS);
      if (Q) {
	Domain_Free(P);
	P=Q;
      }

      if (emptyQ(P)) 
	return ZPU;

      assert (value_notzero_p (P->Constraint[0][0]));
      ChooseOrder (&P, NbExi);
      Rdark = Project (P, NbExi);
      Coprime_P = Rdark->P;
      NonCoprime_P = Rdark->UZP;
      ZPU = NonCoprime_P;
      M = Matrix_Alloc (P->Dimension, P->Dimension);

      for (i = 0; i < M->NbRows; i++)
	value_set_si (M->p[i][i], 1);

      if (Coprime_P != NULL && !emptyQ (Coprime_P)) {
	TP1 = Coprime_P;

	while (TP1) {
	  TP2 = TP1->next;
	  TP1->next = NULL;
	  LU = LatticeUnion_Alloc ();
	  LU->M = Matrix_Copy (M);
	  ZTP1 = UZPolyhedron_Set (LU, TP1);
	  ZTP1->next = ZPU;
	  ZPU = ZTP1;
	  TP1 = TP2;
	}
      }

   
      NbExi--;
   
      if (NbExi != 0)
	{
#pragma omp parallel
	  {
#pragma omp master
	    {
	      while (NbExi > 0)
		{
		  ZPU1 = NULL;

		  for (ZTP1 = ZPU; ZTP1; ZTP1 = ZTP1->next)
		    {
	
#pragma omp task default(none) firstprivate(ZTP1, Q) shared(ZPU1, ZPU, using_one_dim, NbPoly, NbExi, single)
		      {
		
			Polyhedron *R, *S, *TS, *TP;
			LatticeUnion *Ltu, *LU=NULL;
			int bl, cont;
			ZPolyhedron *ZZP;
			UZPolyhedron *ZP, *ZTP2;
			Matrix *mt, *LT, *MT, *M;
			Lattice *L;
			int continue1 = 0, i, j;
			SplitP *Rdark;

			LU = ZTP1->LU;
			cont = 0;

			//#ifdef USING_ONE_DIM
        
			if (using_one_dim && NbPoly == 1 && NbExi == 1 && ZPU->next == NULL)
			  {
			    if (NbTests (ZPU->P) > 20) // for example
			      {	 		
				single = 1;

				R = ProjectOneDim (ZPU->P, 1);
				S = R;
				while (S)
				  {
				    TP = S->next;
				    S->next = NULL;
				    Ltu = LatUnionCopy (LU);
#pragma omp critical
				    {
				      ZP = UZPolyhedron_Set (Ltu, S);
				      ZP->next = ZPU1;
				      ZPU1 = ZP;
				    }
				    S = TP;
				  }
				//  continue;
				continue1 = 1;
			      }
			  }
			//#endif
	        
			if (continue1 == 0) 
			  {
			    while (LU)
			      {
		        
				if (cont)
				  break;
				L = LU->M;
				UpTriangurLattice (&L);

				Q = Polyhedron_Preimage (ZTP1->P, L, MAXRAYS);
				//PolyhedronSimplify (&Q, MAXRAYS);
				R = Polyhedron_Trancate2 (Q, MAXRAYS);
				if (R) {
				  Domain_Free(Q);
				  Q=R;
				}
        

				if (emptyQ (Q))
				  {
				    cont = 1;
				    continue;
				  }
				unsigned d = Q->Dimension;
				unsigned nbe = Q->NbEq;
				if (nbe != 0)
				  {
				    unsigned nbeq = 1;
				    assert (value_zero_p (Q->Constraint[0][0]));
				    bl = 1;
				    if (value_zero_p (Q->Constraint[0][1]))
				      {
					bl = 0;
					for (i = 1; i < Q->NbConstraints; i++)
					  if (value_zero_p (Q->Constraint[i][0]))
					    nbeq++;
				      }
				    if (!bl)
				      {
					mt = Matrix_Alloc (Q->NbConstraints - nbeq, d + 2);
					for (i = 0; i < mt->NbRows; i++)
					  Vector_Copy (Q->Constraint[i + nbeq], mt->p[i], d + 2);
					R = Constraints2Polyhedron (mt, MAXRAYS);
					assert (value_one_p (R->Constraint[0][0]));	
					Matrix_Free (mt);
					mt = Matrix_Alloc (nbeq, d + 1);
					for (i = 0; i < nbeq; i++)
					  {
					    value_assign (mt->p[i][0],
							  Q->Constraint[i][0]);
					    Vector_Copy (Q->Constraint[i] + 2, mt->p[i] + 1, d);
					  }
					TP = Constraints2Polyhedron (mt, MAXRAYS);
					Matrix_Free (mt);
					MT = Matrix_Alloc (d, d);
					for (i = 0; i < MT->NbRows; i++)
					  {
					    for (j = 0; j < MT->NbRows; j++)
					      {
						value_assign (MT->p[i][j], L->p[i + 1][j + 1]);
					      }
					  }

					if (ZPU->next == NULL)
					  ChooseOrder (&R, NbExi);
					PolyhedronSimplify (&R, MAXRAYS);

					Q = Polyhedron_Trancate2 (R, MAXRAYS);
					if (Q) {
					  Domain_Free(R);
					  R=Q;
					}
				  
					Rdark = Project (R, NbExi);
						Domain_Free (R);
					if (Rdark->P != NULL && !emptyQ (Rdark->P))
					  {
					    R = DomainIntersection (Rdark->P, TP, MAXRAYS);
					    for (S = R; S; S = S->next)
					      {
						TS = Polyhedron_Image (S, MT, MAXRAYS);
						Ltu = LatticeUnion_Alloc ();
						Ltu->M = Matrix_Copy (MT);
#pragma omp critical
						{
						  ZP = UZPolyhedron_Set (Ltu, TS);
						  ZP->next = ZPU1;
						  ZPU1 = ZP;
						}
					      }
					    	    Domain_Free (R);
					  }
					for (ZTP2 = Rdark->UZP; ZTP2; ZTP2 = ZTP2->next)
					  {
					    for (Ltu = ZTP2->LU; Ltu; Ltu = Ltu->next)
					      {
						LT = Ltu->M;
						Ltu->M = LatticeImage (Ltu->M, MT);
						Matrix_Free (LT);
					      }
					    R = DomainIntersection (ZTP2->P, TP, MAXRAYS);
					    if (R && !emptyQ (R))
					      {
						S = Polyhedron_Image (R, MT, MAXRAYS);
						Domain_Free (R);
						Ltu = LatUnionCopy (ZTP2->LU);
#pragma omp critical
						{
						  ZP = UZPolyhedron_Set (Ltu, S);
						  ZP->next = ZPU1;
						  ZPU1 = ZP;
						}
					      }
					  }
					if (Rdark->P == NULL || emptyQ (Rdark->P))
					  Matrix_Free (MT);
				      }
				    else
				      {
					int nbexist = 1;
					ZZP = Poly2Zpoly (Q, &nbexist, Q->Dimension - 1);

					if (ZZP != NULL)
					  {
					    Domain_Free (Q);	 
					    Q = Polyhedron_Preimage (ZZP->P, ZZP->Lat, MAXRAYS);
					    M = Matrix_Alloc (d, d);
					    MT = Matrix_Alloc (d, d);
					    for (i = 0; i < M->NbRows; i++)
					      {
						for (j = 0; j < M->NbRows; j++)
						  {
						    value_assign (M->p[i][j], ZZP->Lat->p[i][j]);
						    value_assign (MT->p[i][j], L->p[i + 1][j + 1]);
						  }
					      }
					    R = Polyhedron_Image (Q, M, MAXRAYS);
					    Domain_Free (Q);
					    Q = Polyhedron_Image (R, MT, MAXRAYS);
					    Ltu = LatticeUnion_Alloc ();
					    Ltu->M = LatticeImage (M, MT);
#pragma omp critical
					    {
					      ZP = UZPolyhedron_Set (Ltu, Q);
					      ZP->next = ZPU1;
					      ZPU1 = ZP;
					    }
					    Matrix_Free (MT);
					    Matrix_Free (M);
					  }
				      }
				  }
				else
				  {
				    MT = Matrix_Alloc (d, d);
				    for (i = 0; i < MT->NbRows; i++)
				      {
					for (j = 0; j < MT->NbRows; j++)
					  {
					    value_assign (MT->p[i][j], L->p[i + 1][j + 1]);
					  }
				      }
		        
				    if (ZPU->next == NULL)
				      ChooseOrder (&Q, NbExi);

				    Rdark = Project (Q, NbExi);
				    Polyhedron *S;
				    if (Rdark->P != NULL && !emptyQ (Rdark->P))
				      {
					Q = Rdark->P;
					while (Q)
					  {
					    S = Q->next;
					    Q->next = NULL;
					    if (!emptyQ (Q))
					      {	
						R = Polyhedron_Image (Q, MT, MAXRAYS);
						Ltu = LatticeUnion_Alloc ();
						Ltu->M = Matrix_Copy (MT);
#pragma omp critical
						{
						  ZP = UZPolyhedron_Set (Ltu, R);
						  ZP->next = ZPU1;
						  ZPU1 = ZP;
						}
					      }
					    Domain_Free (Q);
					    Q = S;
					  }
				      }
				    for (ZTP2 = Rdark->UZP; ZTP2; ZTP2 = ZTP2->next)
				      {
					for (Ltu = ZTP2->LU; Ltu; Ltu = Ltu->next)
					  {
					    LT = Ltu->M;
					    Ltu->M = LatticeImage (Ltu->M, MT);
					    Matrix_Free (LT);
					  }
					R = Polyhedron_Image (ZTP2->P, MT, MAXRAYS);
					Ltu = LatUnionCopy (ZTP2->LU);
#pragma omp critical
					{
					  ZP = UZPolyhedron_Set (Ltu, R);
					  ZP->next = ZPU1;
					  ZPU1 = ZP;
					}
				      }
				    if (Rdark->UZP)
				      UZDomain_Free (Rdark->UZP);
				    free (Rdark);
				    Matrix_Free (MT);
				  }
				LU = LU->next;
			      }
			  }
		      }
		    }
#pragma omp taskwait

		  UZDomain_Free (ZPU);
		  ZPU = ZPU1;
		  NbExi--;
		}
	    }
	  }
	}
 
    }
  }
  
    

  if (ZPU != NULL) {
    if (PE) { 
      if (single) {
	unsigned dz=ZPU->P->Dimension;
	if (PE->Dimension != dz) {
	  int dt=PE->Dimension;
	  MT=Matrix_Alloc(PE->NbConstraints, dz+2);
	  for(i=0; i < PE->NbConstraints; i++) {
	    value_assign(MT->p[i][0], PE->Constraint[i][0]), 
	      Vector_Copy (PE->Constraint[i]+1, MT->p[i]+(dz-dt+1), PE->Dimension + 1);
	  }
	  Domain_Free(PE);
	  PE=Constraints2Polyhedron(MT,MAXRAYS);		    
	  Matrix_Free(MT);
	}
      }		    
      for (ZTP1 = ZPU; ZTP1; ZTP1 = ZTP1->next) {
	Q = ZTP1->P;
	ZTP1->P = DomainIntersection (ZTP1->P, PE, MAXRAYS);
	Domain_Free (Q);
      }
    }

    if (MM != NULL) {
      //#ifdef USING_ONE_DIM
      if (using_one_dim){
	if (single) { 
	  Matrix *Ms = Matrix_Alloc (ZPU->P->Dimension + 1, ZPU->P->Dimension + 1);
	  int k = Ms->NbRows - MM->NbRows;
	  for (i = 0; i <= ZPU->P->Dimension; i++)
	    value_set_si (Ms->p[i][i], 1);
	  for (i = 0; i < MM->NbRows - 1; i++)
	    for (j = 0; j < MM->NbColumns; j++)
	      value_assign (Ms->p[i + k][j + k], MM->p[i][j]);
	  Matrix_Free (MM);
	  MM = Matrix_Copy (Ms);
	  Matrix_Free (Ms);
	}
      }
      //#endif
      for (ZTP1 = ZPU; ZTP1; ZTP1 = ZTP1->next) {
	for (LU = ZTP1->LU; LU; LU = LU->next) {
	  L = LU->M;
	  LU->M = LatticeImage (LU->M, MM);
	  Matrix_Free (L);
	}
	Q = ZTP1->P;
	ZTP1->P = Polyhedron_Image (ZTP1->P, MM, MAXRAYS);
	Domain_Free (Q);
      }
    }
  }

  // printing result
  /* i=0;
     for (ZTP1 = ZPU; ZTP1; ZTP1 = ZTP1->next) i++;
     printf("\nnumber of Zpolytopes: %d\n", i); 
  */

  return ZPU;
}
